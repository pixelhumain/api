<?php

namespace PixelHumain\PixelHumain\modules\api\components;

use Authorisation;
use Controller;
use PixelHumain\PixelHumain\extensions\CornerDev;
use Log;
use Yii;

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ApiController extends Controller
{
  public $version = "v0.1.1";
  public $versionDate = "12/01/2017 15:00";
  public $title = "API Communecter";
  public $subTitle = "se connecter à sa commune";
  public $pageTitle = "API Communecter, se connecter à sa commune";
  public static $moduleKey = "api";
  public $keywords = "connecter, réseau, sociétal, citoyen, société, regrouper, commune, communecter, social";
  public $description = "Communecter : Connecter à sa commune, réseau sociétal, le citoyen au centre de la société.";
  
  public $notifications = array();
  //TODO - Faire le tri des liens
  //TODO - Les children ne s'affichent pas dans le menu
 
  public $pages = array(
    "data"=> array(
        "get"              => array("href" => "/ph/api/data/get", "public" => true),
        "tags"              => array("href" => "/ph/api/data/tags", "public" => true),
    ),
    "default"=> array(
        "index"              => array("href" => "/ph/api/default/index", "public" => true),
    ),
    "person"=> array(
        "get"                 => array("href" => "/ph/api/person/get", "public" => true),
        "test"                 => array("href" => "/ph/api/token/index", "public" => true),
        //"authenticate"        => array("href" => "/ph/api/person/authenticate",'title' => "Authentication"),
        //'changepassword'      => array("href" => "/ph/api/person/changepassword"),
        //"register"            => array("href" => "/ph/api/person/register"),
        //'getuseridbymail'     => array("href" => "/ph/api/getuseridbymail"),
        //"updatefield"         => array("href" => "/ph/api/person/updatefield"),
    ),
    "activitypub" => array(
        "get"                 => array("href" => "/ph/api/activitypub/get", "public" => true),
        "actor"               => array("href"=> "/ph/api/abc/person/actor", "public" => true)
    ),
    "token" => array(
      "test"                 => array("href" => "/ph/api/token/index", "public" => true),
  ),
    "organization"=> array(
        "get"                   => array("href" => "/ph/api/organization/get", "public" => true),
     
    ),
    "answer"=> array(
        "get"                   => array("href" => "/ph/api/answer/get", "public" => true),
     
    ),
    "event"=> array(
        "get"                 => array("href" => "/ph/api/event/get", "public" => true),
    ),
    "project"=> array(
        "get"                 => array("href" => "/ph/api/project/get", "public" => true),
    ),
    "link"=> array(
        /*'connect'             => array("href" => "/ph/api/link/connect", "public" => true),
        'follow'              => array("href" => "/ph/api/link/follow", "public" => true),
        'disconnect'          => array("href" => "/ph/api/link/disconnect", "public" => true),
        "removemember"        => array("href" => "/ph/api/link/removemember"),
        "removecontributor"   => array("href" => "/ph/api/link/removecontributor"),
        "validate"            => array("href" => "/ph/api/link/validate"),*/
    ),
    "news"=> array(
      'get'             => array("href" => "/ph/api/news/get", "public" => true),
      'getpagecontent' => array("href" => "/ph/api/news/getpagecontent", "public" => true),
      /*"save"            => array( "href" => "/ph/api/news/save"),
      "delete"          => array( "href" => "/ph/api/news/delete"),
      "updatefield"     => array( "href" => "/ph/api/news/updatefield"),*/
      
    ),
    "comment"=> array(
      /*"save"            => array( "href" => "/ph/api/comment/save"),
      'abuseprocess'    => array( "href" => "/ph/api/comment/abuseprocess"),
      "delete"          => array( "href" => "/ph/api/comment/delete"),
      "updatefield"     => array( "href" => "/ph/api/comment/updatefield"),*/
    ),
    "need"=> array(
        /*"saveneed"              => array("href" => "/ph/api/need/saveneed"),
        "updatefield"           => array("href" => "/ph/api/need/updatefield"),
        "addhelpervalidation"   => array("href" => "/ph/api/need/addhelpervalidation"),*/
        
      ),
    "city"=> array(
      'get'                     => array("href" => "/ph/api/city/get", "public" => true),
      /*'getcitydata'             => array("href" => "/ph/api/city/getcitydata", "public" => true),
      'getcityjsondata'         => array("href" => "/ph/api/city/getcityjsondata", "public" => true),
      'getoptiondata'           => array("href" => "/ph/api/city/getoptiondata"),
      'getlistoption'           => array("href" => "/ph/api/city/getlistoption"),
      'getpodopendata'          => array("href" => "/ph/api/city/getpodopendata"),
      'addpodopendata'          => array("href" => "/ph/api/city/addpodopendata"),
      'getlistcities'           => array("href" => "/ph/api/city/getlistcities"),
      'updatecitiesgeoformat'   => array("href" => "/ph/api/city/updatecitiesgeoformat","public" => true),
      'getinfoadressbyinsee'    => array("href" => "/ph/api/city/getinfoadressbyinsee"),
      'cityexists'              => array("href" => "/ph/api/city/cityexists"),
      'autocompletemultiscope'  => array("href" => "/ph/api/city/autocompletemultiscope"),*/
    ),
    "tool"=> array(
      'get'             => array("href" => "/ph/api/tool/get", "public" => true),
      'datetime'                => array("href" => "/ph/api/tool/datetime", "public" => true),
    ),
    "tags"=> array(
      'get'             => array("href" => "/ph/api/tags/get", "public" => true),
      'search'             => array("href" => "/ph/api/tags/search", "public" => true),
    ),
    "convert" => array(
      "index"              => array("href" => "/ph/api/convert/index", "public" => true),
      "geojson"            => array("href" => "/ph/api/convert/geojson", "public" => true),
      "wikipedia"            => array("href" => "/ph/api/convert/wikipedia", "public" => true),
      "idwikidata"            => array("href" => "/ph/api/convert/idwikidata", "public" => true),
      "datagouv"            => array("href" => "/ph/api/convert/datagouv", "public" => true),
      "osm"            => array("href" => "/ph/api/convert/osm", "public" => true),
      "ods"            => array("href" => "/ph/api/convert/ods", "public" => true),
      "datanova"       => array("href" => "/ph/api/convert/datanova", "public" => true),
      "poleemploi"     => array("href" => "/ph/api/convert/poleemploi", "public" => true),
      "educ"           => array("href" => "/ph/api/convert/educ", "public" => true),
      "educmembre"           => array("href" => "/ph/api/convert/educmembre", "public" => true),
      "educecole"           => array("href" => "/ph/api/convert/educecole", "public" => true),
      "educstruct"           => array("href" => "/ph/api/convert/educstruct", "public" => true),
      "educetab"           => array("href" => "/ph/api/convert/educetab", "public" => true),
      "valueflows"           => array("href" => "/ph/api/convert/valueflows", "public" => true),
      "organcity"            => array("href" => "/ph/api/convert/organcity", "public" => true),
      "gogocarto"            => array("href" => "/ph/api/convert/gogocarto", "public" => true),
      "ftl"            => array("href" => "/ph/api/convert/ftl", "public" => true),

      "get"            => array("href" => "/ph/api/convert/get", "public" => true),
      ),
    "poi"=> array(
        "get"                   => array("href" => "/ph/api/poi/get", "public" => true),
    ),
  );

  function initPage(){
    
    //review the value of the userId to check loosing session
    //creates an issue with Json requests : to clear add josn:true on the page definition here 
    //if( Yii::app()->request->isAjaxRequest && (!isset( $page["json"] )) )
      //echo "<script type='text/javascript'> userId = '".Yii::app()->session['userId']."'; var blackfly = 'sosos';</script>";
    
    //managed public and private sections through a url manager
    $page = $this->pages[Yii::app()->controller->id][Yii::app()->controller->action->id];
    
    $headers = getallheaders();
    if( isset($headers["X-Auth-Token"]) && isset($headers["X-User-Id"]) && isset($headers["X-Auth-Name"]) && Authorisation::isMeteorConnected( $headers["X-Auth-Token"], $headers["X-User-Id"], $headers["X-Auth-Name"] ) ){
      $prepareData = false;
    } else if (isset($headers['Authorization']) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) {
      Authorisation::isJwtconnected($matches[1]);
    }

    CornerDev::addWorkLog("communecter","you@dev.com",Yii::app()->controller->id,Yii::app()->controller->action->id);
    
  }
  public function beforeAction($action){

    Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
    Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');

    // si la requête est de type OPTIONS, on retourne une réponse 200
    if($_SERVER[ 'REQUEST_METHOD' ] === 'OPTIONS'){
        Yii::$app->response->setStatusCode(200);
        return Yii::$app->response->send();
    }
    
    if( $_SERVER['SERVER_NAME'] == "127.0.0.1" || $_SERVER['SERVER_NAME'] == "localhost" ){
      Yii::app()->assetManager->forceCopy = getenv("YII_prod") ? false : true;
    }

    $this->manageLog();

    return parent::beforeAction($action);
  }

  /**
   * Start the log process
   * Bring back log parameters, then set object before action and save it if there is no return
   * If there is return, the method save in session the log object which will be finished and save in db during the method afteraction
   */
  protected function manageLog(){
    //Bring back logs needed
    $actionsToLog = Log::getActionsToLog();
    $actionInProcess = Yii::app()->controller->id.'/'.Yii::app()->controller->action->id;

    //Start logs if necessary
    $headers = getallheaders();
    if( isset( $headers["X-Auth-Name"] ) && in_array( $headers["X-Auth-Name"], ["franceTiersLieux"] ) ) {
      Log::increment( $actionInProcess, Yii::app()->session[ "userId" ] );
    }
    else if( isset($actionsToLog[$actionInProcess]) ) {

      //To let several actions log in the same time
      if(!$actionsToLog[$actionInProcess]['waitForResult']){
        Log::save(Log::setLogBeforeAction($actionInProcess));
      } else if(isset(Yii::app()->session["logsInProcess"]) && is_array(Yii::app()->session["logsInProcess"])){
        Yii::app()->session["logsInProcess"] = array_merge(
          Yii::app()->session["logsInProcess"],
          array($actionInProcess => Log::setLogBeforeAction($actionInProcess))
        );
      } else{
         Yii::app()->session["logsInProcess"] = array($actionInProcess => Log::setLogBeforeAction($actionInProcess));
      }
    }
  }
}

