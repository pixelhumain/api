<?php

use PixelHumain\PixelHumain\components\ThemeHelper;

/**
 * Api Module
*/

class ApiModule extends \yii\base\Module
{

	private $_version = "v0.1.0";
	private $_versionDate = "20/09/2019";
	private $_keywords = "module,opensource,CO,communecter,api";
	private $_description = "oPEN Data an open systems";
	private $_pageTitle = "CO api";
	public $controllerNamespace = 'PixelHumain\PixelHumain\modules\api\controllers';

	public function getVersion(){return $this->_version;}
	public function getVersionDate(){return $this->_versionDate;}
	public function getKeywords(){return $this->_keywords;}
	public function getDescription(){return $this->_description;}
	public function getPageTitle(){return $this->_pageTitle;}

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		Yii::app()->setComponents(array(
		    'errorHandler'=>array(
		        'errorAction'=>'/'.$this->id.'/error'
		    )
		));
		
		Yii::app()->homeUrl = Yii::app()->createUrl($this->id);
		ThemeHelper::setWebsiteTheme($this->getTheme());

		Yii::app()->language = (isset(Yii::app()->session["lang"])) ? Yii::app()->session["lang"] : 'fr';
		
		// import the module-level models and components
		Yii::app()->params["module"] = array(
			"name" => self::getPageTitle(),
			"parent" => "co2",
			"costumId"=>"",
			"overwrite" => array(
				"views" => array(),
				"assets" => array(),
				"controllers" => array(),
			));
	}

	private $_assetsUrl;
	public function getParentAssetsUrl()
	{
		return ( @Yii::app()->params["module"]["parent"] ) ?  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()  : self::getAssetsUrl();
	}
	
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null)
	        $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
	            Yii::getPathOfAlias($this->id.'.assets') );
	    return $this->_assetsUrl;
	}

	public function getTheme() {
		$theme = "CO2";
		if (!empty(Yii::app()->params['theme'])) {
			$theme = Yii::app()->params['theme'];
		} else if (empty(Yii::app()->theme)) {
			$theme = "CO2";
		}

		if(@$_GET["tpl"] == "iframesig"){ $theme = $_GET["tpl"]; }
		return $theme;
	}
}
