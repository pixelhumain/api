<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class NotificationController extends ApiController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'getnotifications'    		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\notification\GetAction::class,
	        'marknotificationasread'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\notification\RemoveAction::class,
	        'markallnotificationasread' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\notification\RemoveAllAction::class
	    );
	}
}