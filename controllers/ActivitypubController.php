<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use Controller;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use Yii;
use Authorisation;

class ActivitypubController extends Controller {
	protected $default_url;

	public function actions()
	{
	    return array(
			// EVENTS
			'joinevent'=> \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\event\JoinEventAction::class,
			'leaveevent'=> \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\event\LeaveEventAction::class,
			//COLLECTIONS
            // filtre par tags
            'suggestions' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\GetSuggestionTagsAction::class,

            'outbox' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\OutboxAction::class,
			'followers' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\FollowersAction::class,
			'following' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\FollowingAction::class,
			'users' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\GetActorAction::class,
			'webfinger' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\WebfingerAction::class,
			'inbox' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\InboxAction::class,
			'doactivity' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\DoActivityAction::class,
			'activity' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\GetActivityAction::class,
			'object' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\GetObjectAction::class,
			'getcommunity' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\GetCommunityAction::class,
			'search' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\SearchAction::class,
			'link' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\LinkAction::class,
			'processcron' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\ProcessCron::class,
			'notereplies' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\GetNoteRepliesAction::class,
			'comment' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\CommentAction::class,
			// GROUPS
			'groups' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\GetGroupAction::class,
			'host-meta' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\GetHostMetaAction::class,
			// PROJECTS
			'projects' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\GetProjectAction::class,
			'manageroles' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\project\ManageRolesAction::class,
			// test activity for dev
			'test' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\test\ActivityTestAction::class,
			'contributors' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\project\ContributorsAction::class,
			//'projects' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\project\GetProjectByActorAction::class,
		    // logs
            'logs' =>  \PixelHumain\PixelHumain\modules\api\controllers\activitypub\logs\GetLogAction::class,
            // dashboard
            'usages' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard\GetActivitypubCommunityAction::class,
            'getactivitybydaterange' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard\GetActivityByDateRangeAction::class,
            'getinstanceinteracting' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard\GetInstanceInteractingByDateRangeAction::class,
            'getinteractions' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard\GetInteractionByDateRangeAction::class,
            'getactivitiesbytype' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard\GetActivitiesByTypeAndDateRangeAction::class,
            'getobjectcountbytype' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard\GetObjectCountByTypeAction::class,
            'getactivitybymonthrange' => \PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard\GetActivityByMonthRangeAction::class

        );
	}

	public function setDefaultUrl($value){
		$this->default_url = $value;
	}

	/* -------------------------------------------------------------------------- */
	/*                            CONTROLLER LIFECICLE                            */
	/* -------------------------------------------------------------------------- */

	public function beforeAction($action){

		parent::beforeAction($action);

        Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
        Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->set('Content-Type', 'application/activity+json');

        // si la requête est de type OPTIONS, on retourne une réponse 200
        if($_SERVER[ 'REQUEST_METHOD' ] === 'OPTIONS'){
            Yii::$app->response->setStatusCode(200);
            return Yii::$app->response->send();
        }

        $headers = getallheaders();
        if (isset($headers['Authorization']) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) {
            $retour = Authorisation::isJwtconnected($matches[1]);
            if(!$retour){
                return $this->errorToken(401, 'Erreur lors de la validation du jeton');
            }
          } else {
            // return $this->errorToken(400);
          }

		$request = Yii::$app->request;
		$headers = $request->headers;
		$accept = explode(";", $headers["accept"])[0];
		$contentTypes = explode(",", $accept);

		if(preg_match('/^(.*)(\.json)$/', $request->pathinfo))
			return true;

		$authorize = false;
		foreach($contentTypes as $contentType){
			if(str_contains(Request::HEADER_ACCEPT, $contentType)){
				$authorize = true;
				break;
			}
		}

		if(!$authorize && filter_var($this->default_url, FILTER_VALIDATE_URL)){
			return $this->redirect($this->default_url)->send();
		}
		return true;
	}

	public function bindActionParams($action, $params){
		if(isset($params["rest"]) && preg_match('/^(.*)(\.json)$/', $params["rest"])){
			foreach($params as $index => $param)
				$params[$index] = str_replace(".json", "", $param);
		}
		unset($params["rest"]);

		return $params;
	}

	public function errorToken($code = 401, $message = 'Unauthorized: Invalid or missing token') {
        Yii::$app->response->statusCode = $code;

        // Définit le format de réponse et le contenu (optionnel)
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data = ['error' => $message];

        // Envoie la réponse
        return Yii::$app->response->send();
    }
}
