<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use Controller;
use Yii;
use Authorisation;
use yii\filters\Cors;

class CocolightController extends Controller {
    protected $default_url;

    public function actions()
    {
        return array(
            'authenticate'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\AuthenticateAction::class,
            'refreshtoken'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\RefreshTokenAction::class,
            'me'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\MeAction::class,
            'getone'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\GetOneAction::class,
            'getprojectsbyiduser'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\ProjectAction::class,
            'getpoibyiduser'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\PoiAction::class,
            'getorgabyiduser'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\OrgaAction::class,
            'getmulticitoyens'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\GetMultiCitoyensAction::class,
            'infoserver'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\InfoServerAction::class,
            'validateexchangetoken'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\ValidateExchangeTokenAction::class,
            'exchangetoken'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\ExchangeTokenAction::class,
            'validatecreateuser'=> \PixelHumain\PixelHumain\modules\api\controllers\cocolight\ValidateCreateUserAction::class,
        );
    }

    /* -------------------------------------------------------------------------- */
    /*                            CONTROLLER LIFECICLE                            */
    /* -------------------------------------------------------------------------- */

    public function beforeAction($action){

        parent::beforeAction($action);

        Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
        Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');

        // si la requête est de type OPTIONS, on retourne une réponse 200
        if($_SERVER[ 'REQUEST_METHOD' ] === 'OPTIONS'){
            Yii::$app->response->setStatusCode(200);
            return Yii::$app->response->send();
        }

        $headers = getallheaders();
        if (isset($headers['Authorization']) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) {
            $retour = Authorisation::isJwtconnected($matches[1]);
            if(!$retour){
                return $this->errorToken(401, 'Erreur lors de la validation du jeton');
            }     
          } else {
            // return $this->errorToken(400);
          }

        $request = Yii::$app->request;

        $headers = $request->headers;
        $accept = explode(";", $headers["accept"])[0];
        $contentTypes = explode(",", $accept);

        if(preg_match('/^(.*)(\.json)$/', $request->pathinfo))
            return true;

        $authorize = false;
        /**foreach($contentTypes as $contentType){
            if(str_contains(Request::HEADER_ACCEPT, $contentType)){
                $authorize = true;
                break;
            }
        }**/

        if(!$authorize && filter_var($this->default_url, FILTER_VALIDATE_URL)){
            return $this->redirect($this->default_url)->send();
        }
        return true;
    }

    public function bindActionParams($action, $params){
        if(isset($params["rest"]) && preg_match('/^(.*)(\.json)$/', $params["rest"])){
            foreach($params as $index => $param)
                $params[$index] = str_replace(".json", "", $param);
        }
        unset($params["rest"]);

        return $params;
    }
    public function errorToken($code = 401, $message = 'Unauthorized: Invalid or missing token') {
        Yii::$app->response->statusCode = $code;

        // Définit le format de réponse et le contenu (optionnel)
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data = ['error' => $message];

        // Envoie la réponse
        return Yii::$app->response->send();
    }
}
