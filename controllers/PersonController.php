<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class PersonController extends ApiController {

  public $hasSocial = false;
  public $loginRegister = true;

  
  public function accessRules() {
    return array(
      // not logged in users should be able to login and view captcha images as well as errors
      array('allow', 'actions' => array('index','graph','register','register2')),
      // logged in users can do whatever they want to
      array('allow', 'users' => array('@')),
      // not logged in users can't do anything except above
      array('deny'),
    );
  }

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions(){
	    return array(
          'get'                   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\GetAction::class,
          'authenticate'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\AuthenticateAction::class,
          'changepassword'        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\ChangePasswordAction::class,
          'register'              => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\RegisterAction::class,
          'getuseridbymail'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\GetUserIdByMailAction::class,
          'updatefield'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\person\UpdateFieldAction::class,
          /*
          'update'             => 'citizenToolKit.controllers.person.UpdateAction',
          "updatesettings"    => 'citizenToolKit.controllers.person.UpdateSettingsAction',
          */
	    );
	}
}