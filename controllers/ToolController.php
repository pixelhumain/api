<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;


use PixelHumain\PixelHumain\modules\api\components\ApiController;

/**
* Created on examples of other controllers 
* api tool datetime for update rtctime of Smart-Citizen-Kit 
* @author: Jean Daniel CAZAL <danzalkay551@gmail.com>
* Date: 12/01/2017
*/

class ToolController extends ApiController {
	
	public function beforeAction($action) {
    	parent::initPage();
    	return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
          'get'                   	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\tool\GetAction::class,
          'datetime'                   	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\tool\DateTimeAction::class,
          
        );
	}

}
?>