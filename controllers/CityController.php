<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class CityController extends ApiController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	    	'get'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetAction::class,
	        'getcitydata'     		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCityDataAction::class,
	        'getcityjsondata'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCityJsonDataAction::class,
	        'getcitiesdata'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetCitiesDataAction::class,
	        'getoptiondata'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetOptionDataAction::class,
	        'getlistoption'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetListOptionAction::class,
	        'getpodopendata'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetPodOpenDataAction::class,
	        'addpodopendata'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\AddPodOpenDataAction::class,
	        'getlistcities'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetListCitiesAction::class,
	        'updatecitiesgeoformat' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\UpdateCitiesGeoFormatAction::class,
	        'getinfoadressbyinsee'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\GetInfoAdressByInseeAction::class,
	        'cityexists'  			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\CityExistsAction::class,
	        'autocompletemultiscope'=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\city\AutocompleteMultiScopeAction::class,

	    );
	}
}