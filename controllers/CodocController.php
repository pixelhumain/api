<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use Authorisation;
use Citoyen;
use Controller;
use Exception;
use Gitlab\Client;
use PHDB;
use Rest;
use Yii;

class CodocController extends Controller {
    private Client $client;
    private $config = [
        "branch" => "master",
        "project_id" => "",
        "token" => "",
        "url" => ""
    ];
  
    public function beforeAction($action){
        $this->config =  array_merge($this->config, Yii::app()->params['gitlabCodoc']);
        
        $this->client = new Client();
        if(trim($this->config["url"]) !== "")
            $this->client->setUrl($this->config["url"]);
        $this->client->authenticate($this->config["token"], Client::AUTH_HTTP_TOKEN);
        return true;
    }

	public function actionGetRepositories($path = "/"){
        $repositories = [];
        $page = 1;

        do{
            $repositories = array_merge($repositories, $this->client->repositories()->tree($this->config["project_id"], [
                'ref' => $this->config["branch"],
                'per_page' => 50,
                'page' => $page,
                'path' => $path
            ]));

            $link_header = $this->client->getLastResponse()->getHeader('Link');
            $has_more_pages = isset($link_header[0]) && strpos($link_header[0], 'rel="next"') !== false;
            $page++;

        }while($has_more_pages);
        
        return Rest::json($repositories);
    }

    public function actionGetFileRaw($path){
        $raw = $this->client->repositoryFiles()->getRawFile($this->config["project_id"], $path, $this->config["branch"]);

        return Rest::json(["raw" => $raw]);
    }

    public function actionUpdateFile(){
        if(!isset($_SESSION["userId"]) || !Authorisation::isUserSuperAdmin($_SESSION["userId"]))
            return Rest::json(["error" => true, "message" => "Unauthorized user."]);

        if(!isset($_POST["path"]) || !isset($_POST["content"]))
            return Rest::json(["error" => true, "message" => "Missing parameters."]);
        
        $user = PHDB::findOneById(Citoyen::COLLECTION, $_SESSION["userId"]);

        $this->client->repositoryFiles()->updateFile($this->config["project_id"], [
            "file_path" => $_POST["path"],
            "branch" => $this->config["branch"],
            "author_email" => $user["email"],
            "author_name" => $user["username"],
            "content" => $_POST["content"],
            "commit_message" => isset($_POST["message"]) && trim($_POST["message"]) !== "" ? $_POST["message"]:"Update from app codoc."
        ]);

        return Rest::json([]);
    }
}