<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class EventController extends ApiController {

  public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions(){
	    return array(
          'get'                   	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\GetAction::class,
          //'save'           		  	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\SaveAction::class,
          //'updatefield'           	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\UpdateFieldAction::class,

          // A faire sur swagger
          //'update'                	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\UpdateAction::class,
          //'delete' 					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\DeleteAction::class,
          //'removeattendee'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\RemoveAttendeeAction::class,
          //"updatesettings"        	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\UpdateSettingsAction::class,
          //'getcalendar'               => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\event\GetCalendarAction::class,
	    );
	}
}
