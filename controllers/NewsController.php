<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class NewsController extends ApiController {

    public function beforeAction($action) {
    	parent::initPage();
    	return parent::beforeAction($action);
  	}
  	public function actions()
	{
	    return array(
	    	'get'     		=> \PixelHumain\PixelHumain\modules\news\controllers\actions\GetAction::class,
	        'save'     		=> \PixelHumain\PixelHumain\modules\news\controllers\actions\SaveAction::class,
	        'delete'     	=> \PixelHumain\PixelHumain\modules\news\controllers\actions\DeleteAction::class,
	        //'updatefield'   => \PixelHumain\PixelHumain\modules\news\controllers\actions\UpdateFieldAction::class,
			'getpagecontent'=> \PixelHumain\PixelHumain\modules\news\controllers\actions\GetPageContentAction::class,
	        /*'extractprocess' => array (
	            'class'   => 'ext.extract-url-content.ExtractProcessAction',
	            'options' => array(
	                // Tmp dir to store cached resized images 
	                'cache_dir'   => Yii::getPathOfAlias('webroot') . '/assets/',	 
	                // Web root dir to search images from
	                'base_dir'    => Yii::getPathOfAlias('webroot') . '/',
	            )
	        )*/
	    );
	}
}

?>