<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

/**
 * 
 */
class CommentController extends ApiController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'save'					=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\SaveAction::class,
	        'abuseprocess'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\AbuseProcessAction::class,
	        'delete'				=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\DeleteAction::class,
	        'updatefield'			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\comment\UpdateFieldAction::class,
	    );
	}
}

?>