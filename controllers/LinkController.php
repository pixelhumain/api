<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use CommunecterController;

/**
 * LinkController.php
 *
 * Manage Links between Organization, Person, Projet and Event
 *
 * Date: 16/08/2016
 */
class LinkController extends CommunecterController {

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}
	public function actions()
	{
	    return array(	
			'follow' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\FollowAction::class,
			'connect' 			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\ConnectAction::class,
			'disconnect' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\DisconnectAction::class,

			// a faire sur Swagger
			'removemember'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\RemoveMemberAction::class,
	        "removerole"		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\RemoveRoleAction::class,
			'removecontributor' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\RemoveContributorAction::class,
			'disconnect' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\DisconnectAction::class,
			
			'deleteinvitationlink' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\link\DeleteInvitationLinkAction::class
	    );
	}
}

?>