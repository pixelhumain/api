<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use CommunecterController;

/**
 * CtenatController.php
 *
 * CtenatController SPECIFIC api calls 
 *
 */
class CtenatController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		//return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
	        'cter'  => \PixelHumain\PixelHumain\modules\api\controllers\ctenat\CterAction::class
	    );
	}
}
