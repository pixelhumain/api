<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class AnswerController extends ApiController {

  public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions(){
	    return array(
          'get'                   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\answer\GetAction::class,
          //'save'                  => 'citizenToolKit.controllers.organization.SaveAction',
          //'updatefield'           => 'citizenToolKit.controllers.organization.UpdateFieldAction',

          /*'update'            => 'citizenToolKit.controllers.organization.UpdateAction',
          'join'              => 'citizenToolKit.controllers.organization.JoinAction',
          
          'declaremeadmin'          => 'citizenToolKit.controllers.organization.DeclareMeAdminAction',
          "updatesettings"        => 'citizenToolKit.controllers.organization.UpdateSettingsAction'*/
	    );
	}
}

?>