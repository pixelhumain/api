<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

/**
 * DefaultController.php
 *
 * azotlive application
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 18/07/2014
 */
class ErrorController extends ApiController {

  public function beforeAction($action)
	{
	  return parent::beforeAction($action);
	}

  public function actions() {
      return array(
      'index'     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\error\ApiAction::class
      );
  }

}