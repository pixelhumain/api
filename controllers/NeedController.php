<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class NeedController extends ApiController {

    public function beforeAction($action) {
    	parent::initPage();
    	return parent::beforeAction($action);
  	}
  	public function actions()
	{
	    return array(
			'saveneed'       		=> 'citizenToolKit.controllers.need.SaveNeedAction',
			'updatefield'       	=> 'citizenToolKit.controllers.need.UpdateFieldAction',
			'addhelpervalidation'   => 'citizenToolKit.controllers.need.AddHelperValidationAction',
	    );
	}
}


?>