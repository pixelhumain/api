<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class ProjectController extends ApiController {

  public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions(){
	    return array(
          'get'                   	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\project\GetAction::class,
          //'updatefield'           	=> 'citizenToolKit.controllers.project.UpdateFieldAction',
          //'save'					=> 'citizenToolKit.controllers.project.SaveAction',

          // A faire sur swagger
          /*'update'					=> 'citizenToolKit.controllers.project.UpdateAction',
          'savecontributor'			=> 'citizenToolKit.controllers.project.SaveContributorAction',
          'editchart'				=> 'citizenToolKit.controllers.project.EditChartAction',
          "updatesettings" 			=> 'citizenToolKit.controllers.project.UpdateSettingsAction'
          */
	    );
	}
}

?>