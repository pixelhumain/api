<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;


use PixelHumain\PixelHumain\modules\api\components\ApiController;

class PoiController extends ApiController {

    public function beforeAction($action) {
    	parent::initPage();
    	return parent::beforeAction($action);
  	}
  	public function actions()
	{
	    return array(
	    	// 'get'     		=> 'citizenToolKit.controllers.poi.GetAction',
			'get' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetAction::class
	    );
	}
}

?>