<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;


use PixelHumain\PixelHumain\modules\api\components\ApiController;

class ConvertController extends ApiController {
  
	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	       	//'index' 	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\IndexAction::class,
	       	//'get' 		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\GetAction::class,
	       	'geojson'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\GeoJsonAction::class,
	       	'wikipedia'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\WikipediaAction::class,
			'idwikidata'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\IdWikidataAction::class,
	       	'datagouv'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\DatagouvAction::class,
	       	'osm'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\OsmAction::class,
	       	'ods'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\OdsAction::class,
	       	'datanova' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\DatanovaAction::class,
	       	'poleemploi' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\PoleEmploiAction::class,
	       	'educmembre' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\EducMembreAction::class,
	       	'educecole' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\EducEcoleAction::class,
	       	'educstruct' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\EducStructAction::class,
	       	'educetab' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\EducEtabAction::class,
	       	'valueflows' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\ValueFlowsAction::class,
	       	'organcity' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\OrgancityAction::class,
	       	'gogocarto'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\GogocartoAction::class,
	       	'ftl'   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\convert\FtlAction::class,

	    );
	}
}



