<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\modules\api\components\ApiController;

class DataController extends ApiController {

  public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions(){
	    return array(
          'get' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\opendata\GetDataSwaggerAction::class, 
		  'tags' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\tags\GetAction::class,
		  'search' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\tags\SearchAction::class
	    );
	}


}

?>