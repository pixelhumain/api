<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\ActionWithTokenValidation;
use Rest;
use PHDB;
use MongoId;
use Yii;
use Organization;

class OrgaAction extends ActionWithTokenValidation
{
    public function run()
    {
        if(Yii::app()->session["userId"]){
            $id = $_POST["id"];
            $result = PHDB::find(Organization::COLLECTION, array("links.members.656f83c8e6c2b0757101e5c2.isAdmin"=> true));
     
             return Rest::json(
                 [
                     "count" => count($result),
                     "result" => $result
                 ]
             );
        } else {
            return $this->controller->errorToken(200, "not connected");
        }
        

    }
}

