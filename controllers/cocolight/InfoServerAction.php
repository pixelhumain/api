<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use Rest;
use PixelHumain\PixelHumain\components\Action;

class InfoServerAction extends Action
{
    public function run()
    {
        $response = [
            "result" => "true"
        ];

        return Rest::json($response);
    }
}