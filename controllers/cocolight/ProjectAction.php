<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\ActionWithTokenValidation;
use Rest;
use PHDB;
use MongoId;
use Project;
use Yii;

class ProjectAction extends ActionWithTokenValidation
{
    public function run()
    {
        if(Yii::app()->session["userId"]){
            $id = $_POST["id"];
            $result = PHDB::find(Project::COLLECTION, array(
                '$or' => array(
                    array("links.contributors.$id" => array('$exists' => true)),
                    array("parent.$id" => array('$exists' => true))
                )
            ));
    
            return Rest::json(
                [
                    "count" => count($result),
                    "result" => $result
                ]
            );
        } else {
            return $this->controller->errorToken(200, "not connected");
        }


    }
}

