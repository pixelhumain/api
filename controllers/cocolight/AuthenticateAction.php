<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;
use Exception;
use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\JWT;
use Rest;
use Person;
use DateTimeImmutable;
use Yii;
use PHDB;
use MongoId;

class AuthenticateAction  extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id =null)
    {
        try {
            $data = json_decode(Yii::$app->getRequest()->getRawBody(), true);
            if(isset($data["email"]) && isset($data["password"])){
                $credentials = $this->checkCredentials($data["email"], $data["password"]);

                if ($credentials['status'] == 201){
                    $date = new DateTimeImmutable();
                    $expire_at = $date->modify('+10 minutes')->getTimestamp();
                    $request_data = [
                        'iat' => $date->getTimestamp(),
                        'iss' => Yii::app()->params['jwt']['domain'],
                        'nbf' => $date->getTimestamp(),
                        'exp' => $expire_at,
                        'id' => $credentials['data']['id'],
                    ];
                    $expire_at_refresh_token = $date->modify('+30 days')->getTimestamp();
                    $request_data_refresh_token = [
                        'iat' => $date->getTimestamp(),
                        'iss' =>  Yii::app()->params['jwt']['domain'],
                        'nbf' => $date->getTimestamp(),
                        'exp' => $expire_at_refresh_token, 
                        'id' => $credentials['data']['id']
                    ];
                    $accessToken = JWT::encode(
                        $request_data,
                        Yii::app()->params['jwt']['secretKey'],
                        Yii::app()->params['jwt']['alg']
                    );
                    $refreshToken = JWT::encode(
                        $request_data_refresh_token,
                        Yii::app()->params['jwt']['refreshTokenKey'],
                        Yii::app()->params['jwt']['alg']
                    );
                    
                    // je commente car autrement il y a un refresh Token valide donc si on à plusieur device on peut se connecter avec le meme compte
                    // PHDB::update('citoyens', array("_id" => new MongoId($credentials['data']['id'])), array('$set' => array("refreshToken"=> $refreshToken )));

                    return Rest::json(array(
                        'user' => $credentials['data'],
                        'accessToken' => $accessToken,
                        'refreshToken' => $refreshToken,
                        'status' => $credentials['status']
                    ),null, true, 'application/json; charset=utf-8');
                } else {
                    return $this->controller->errorToken(401, $credentials['message']);
                }
            } else {
                return $this->controller->errorToken(404, "L'email et le mot de passe pas fourni");
            }
        } catch (Exception $e) {
            return $this->controller->errorToken(500, $e->getMessage());
        }
    }
    private function checkCredentials($email,$password){
        try{

            $timezone= date_default_timezone_get();
            $remember = false;
            if($remember){
                // Todo: remember me
            }
            $res = Person::login( $email , $password, false);

            if($res["result"]){
                return array(
                    'data' => array(
                        'id' => $res['id'],
                        'username' => $res['account']['username'],
                        'email' => $res['account']['email'],
                        'roles' => $res['account']['roles'],
                        'timezone' => $timezone,
                        'lastLoginDate' => $res['account']['lastLoginDate'],
                        'profilImageUrl' => isset($res['account']['profilImageUrl']) ? Yii::app()->params['jwt']['schema'] . "://" . Yii::app()->params['jwt']['domain']. $res['account']['profilImageUrl']: '',

                    ),
                    'status' => 201,
                    'message' => $res['msg']
                );
            }else{
                return array('data' => array(),'status' => 404, 'message' => "L'email ou le mot de passe est incorrect.");
            }

            // Todo: redirect invitation link ref
        }catch (Exception $e) {
             http_response_code(404);
            return array('data' => array(),'status' => 500, 'message' => $e->getMessage());
        }
    }
}



