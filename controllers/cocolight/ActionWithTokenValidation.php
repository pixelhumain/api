<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use Exception;
use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\JWT;
use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\Key;
use Yii;

class ActionWithTokenValidation extends \PixelHumain\PixelHumain\components\Action
{
    protected function validateToken()
    {
        $headers = getallheaders();
        if (isset($headers['Authorization']) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) {
            $jwt_token = $matches[1];
            try {
                $decoded = JWT::decode($jwt_token, new Key(Yii::app()->params['jwt']['secretKey'], Yii::app()->params['jwt']['alg']));
                return $decoded->id;
            } catch (Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

}

