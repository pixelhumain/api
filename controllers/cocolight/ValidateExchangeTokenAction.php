<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use Exception;
use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\JWT;
use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\Key;
use Rest;
use Person;
use PHDB;
use MongoId;
use Yii;

// vérifie la validité de l'exchangeToken et renvoie les informations de l'utilisateur

class ValidateExchangeTokenAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {

        $rawData = Yii::$app->getRequest()->getRawBody();
        $data = json_decode($rawData, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return $this->controller->errorToken(400, "Le corps de la requête doit être un JSON valide");
        }

        // Validation du paramètre 'exchangeToken'
        if (!isset($data["exchangeToken"]) || empty($data["exchangeToken"])) {
            return $this->controller->errorToken(400, "Le jeton d'échange est manquant ou vide");
        }

        if(!isset(Yii::app()->params['jwt']) || !isset(Yii::app()->params['jwt']['domain']) || !isset(Yii::app()->params['jwt']['schema']) || !isset(Yii::app()->params['jwt']['secretKey']) || !isset(Yii::app()->params['jwt']['refreshTokenKey'])){
            return $this->controller->errorToken(500, "Les paramètres de configuration de l'API ne sont pas définis");
        }
        
        // si Yii::app()->params['jwt']['exchangeTokenKey'] n'est pas défini, on ne peut pas valider le token d'échange
        if (!isset(Yii::app()->params['jwt']['exchangeTokenKey']) || empty(Yii::app()->params['jwt']['exchangeTokenKey'])) {
            return $this->controller->errorToken(500, "La clé de validation du token d'échange n'est pas définie");
        }

        $jwt_token = $data["exchangeToken"];

        try {
            $decoded = JWT::decode($jwt_token, new Key(Yii::app()->params['jwt']['exchangeTokenKey'], Yii::app()->params['jwt']['alg']));
            if (isset($decoded->id)) {
                // Validation de l'ID de l'utilisateur
                if (!MongoId::isValid($decoded->id)) {
                    return $this->controller->errorToken(400, "L'ID de l'utilisateur dans le jeton est invalide");
                }

                $user = PHDB::findOne(Person::COLLECTION, ["_id" => new MongoId($decoded->id)], ["_id", "username", "email", "name", "profilImageUrl"]);
        
                if (!$user) {
                    return $this->controller->errorToken(404, "Utilisateur non trouvé");
                }

                // Vérification des champs obligatoires de l'utilisateur
                $requiredFields = ["_id", "username", "email"];
                foreach ($requiredFields as $field) {
                    if (!isset($user[$field]) || empty($user[$field])) {
                        return $this->controller->errorToken(500, "Le champ utilisateur '$field' est manquant ou vide");
                    }
                }
        
                $response = [
                    "valid" => true,
                    "user" => [
                        "id" => (string)$user["_id"],
                        "username" => $user["username"],
                        "email" => $user["email"],
                        "server" => Yii::app()->params['jwt']['domain']
                    ]
                ];
        
                if (isset($user["name"])) {
                    $response["user"]["name"] = $user["name"];
                }
        
                if (isset($user["profilImageUrl"])) {
                    $response["user"]["profilImageUrl"] = Yii::app()->params['jwt']['schema'] . "://" . Yii::app()->params['jwt']['domain'] . $user["profilImageUrl"];
                }
                
                return Rest::json($response, null, true, 'application/json; charset=utf-8');
            } else {
                return $this->controller->errorToken(401, "Le jeton est invalide ou expiré");
            }
        } catch (Exception $e) {
            return $this->controller->errorToken(401, "Le jeton est invalide ou expiré : " . $e->getMessage());
        }
    }
}