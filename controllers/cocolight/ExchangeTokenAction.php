<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\ActionWithTokenValidation;
use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\JWT;

use Rest;
use Person;
use PHDB;
use MongoId;
use Yii;
use DateTimeImmutable;

// Cocolight fait un appel à Communecter.org via /api/cocolight/exchangetoken en envoyant l'accessToken en header et le serverUrl en post (00.re).
// Communecter.org valide l'accessToken, puis génère un exchangeToken.

class ExchangeTokenAction extends ActionWithTokenValidation {
    public function run() {

        $rawData = Yii::$app->getRequest()->getRawBody();
        $data = json_decode($rawData, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return $this->controller->errorToken(400, "Le corps de la requête doit être un JSON valide");
        }

        // Validation du paramètre 'serverUrl'
        if (!isset($data["serverUrl"]) || empty($data["serverUrl"])) {
            return $this->controller->errorToken(400, "Le paramètre 'serverUrl' est manquant ou vide");
        }

        // Validation du format de 'serverUrl'
        if (!filter_var($data["serverUrl"], FILTER_VALIDATE_URL)) {
            return $this->controller->errorToken(400, "Le 'serverUrl' fourni n'est pas une URL valide");
        }

        // on doit aussi verfier qu'il y est Yii::app()->params['jwt'] et Yii::app()->params['jwt']['domain'], Yii::app()->params['jwt']['schema'], Yii::app()->params['jwt']['secretKey'], Yii::app()->params['jwt']['refreshTokenKey']
        if(!isset(Yii::app()->params['jwt']) || !isset(Yii::app()->params['jwt']['domain']) || !isset(Yii::app()->params['jwt']['schema']) || !isset(Yii::app()->params['jwt']['secretKey']) || !isset(Yii::app()->params['jwt']['refreshTokenKey'])){
            return $this->controller->errorToken(500, "Les paramètres de configuration de l'API ne sont pas définis");
        }

        // **Extraction du hostname du 'serverUrl'**
        $serverHostname = parse_url($data["serverUrl"], PHP_URL_HOST);
        if (!$serverHostname) {
            return $this->controller->errorToken(400, "Impossible d'extraire le domaine du 'serverUrl'");
        }

        // **Ajout du contrôle de la liste blanche**
        if (isset(Yii::app()->params['jwt']['whiteListServer']) && is_array(Yii::app()->params['jwt']['whiteListServer']) && !empty(Yii::app()->params['jwt']['whiteListServer'])) {
            $whitelistHostnames = array_map(function($url) {
                // Extraire le hostname de l'URL ou utiliser le domaine directement si pas d'URL
                $hostname = parse_url($url, PHP_URL_HOST);
                return $hostname ? $hostname : $url;
            }, Yii::app()->params['jwt']['whiteListServer']);

            if (!in_array($serverHostname, $whitelistHostnames)) {
                return $this->controller->errorToken(403, "Le 'serverUrl' fourni n'est pas autorisé");
            }
        }
        
        // si Yii::app()->params['jwt']['exchangeTokenKey'] n'est pas défini, on ne peut pas générer de token d'échange
        if (!isset(Yii::app()->params['jwt']['exchangeTokenKey']) || empty(Yii::app()->params['jwt']['exchangeTokenKey'])) {
            return $this->controller->errorToken(500, "La clé de génération du token d'échange n'est pas définie");
        }

        // Vérification du token dans le header et récupération de l'ID de l'utilisateur si le token est valide.
        $id = $this->validateToken();
        if ($id) {
            // Validation de l'ID de l'utilisateur
            if (!MongoId::isValid($id)) {
                return $this->controller->errorToken(400, "L'ID de l'utilisateur est invalide");
            }

            $user = PHDB::findOne(Person::COLLECTION, ["_id" => new MongoId($id)], ["_id", "username", "email", "name", "profilImageUrl"]);

            if (!$user) {
                return $this->controller->errorToken(404, "Utilisateur non trouvé");
            }

            // Vérification des champs obligatoires de l'utilisateur
            $requiredFields = ["_id", "username", "email"];
            foreach ($requiredFields as $field) {
                if (!isset($user[$field]) || empty($user[$field])) {
                    return $this->controller->errorToken(500, "Le champ utilisateur '$field' est manquant ou vide");
                }
            }

            // Génération du jeton d'échange
            $date = new DateTimeImmutable();
            $expire_at = $date->modify('+10 minutes')->getTimestamp();
            $request_data = [
                'iat' => $date->getTimestamp(),
                'iss' => Yii::app()->params['jwt']['domain'],
                'nbf' => $date->getTimestamp(),
                'exp' => $expire_at,
                'id' => $id,
            ];
            $exchangeToken = JWT::encode(
                $request_data,
                Yii::app()->params['jwt']['exchangeTokenKey'],
                Yii::app()->params['jwt']['alg']
            );

            // Préparation des données pour l'appel CURL
            $postData = [
                "exchangeToken" => $exchangeToken,
                "serverUrl" => Yii::app()->params['jwt']['schema'] . "://" . Yii::app()->params['jwt']['domain']
            ];

            // Appel à 00.re via /api/cocolight/validatecreateuser
            $url = rtrim($data["serverUrl"], '/') . "/api/cocolight/validatecreateuser";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if (curl_errno($ch)) {
                $error_msg = curl_error($ch);
                curl_close($ch);
                return $this->controller->errorToken(500, "Erreur lors de la requête CURL : " . $error_msg);
            }

            curl_close($ch);

            if ($http_code !== 200) {
                return $this->controller->errorToken($http_code, "Erreur lors de l'appel à " . $data["serverUrl"] . ": " . $response);
            }

            $decoded_response = json_decode($response, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                return $this->controller->errorToken(500, "Réponse JSON invalide reçue de " . $data["serverUrl"]);
            }

            // Vérification de la présence des tokens dans la réponse
            if (!isset($decoded_response['accessToken']) || !isset($decoded_response['refreshToken'])) {
                return $this->controller->errorToken(500, "Les tokens attendus ne sont pas présents dans la réponse de " . $data["serverUrl"]);
            }

            $exchangeServer = array(
                "serverUrl" => $data["serverUrl"],
                "id" => $decoded_response['user']['id']
            );

            PHDB::update(
                'citoyens',
                array("_id" => new MongoId($id)),
                array('$addToSet' => array("exchangeServers" => $exchangeServer))
            );

            return Rest::json($decoded_response, null, true, 'application/json; charset=utf-8');

        } else {
            return $this->controller->errorToken(401, "Le token est invalide ou expiré");
        }
    }
}