<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\ActionWithTokenValidation;
use Rest;
use Person;
use PHDB;
use MongoId;
use Yii;

class GetMultiCitoyensAction extends ActionWithTokenValidation {
    public function run() {

        if(Yii::app()->session["userId"]){
            if(isset($_POST["id"]) && !empty($_POST["id"])) {
                // Diviser la chaîne $_POST["id"]
                $ids = explode(",", $_POST["id"]);
                $results = [];
                foreach($ids as $citoyenId) {
                    $result = PHDB::find(Person::COLLECTION, array("_id" => new MongoId($citoyenId)));
                    $results[] = $result;
                }
        
                return Rest::json(
                    [
                        "count" => count($results),
                        "result" => $results
                    ]
                );
            } else {
                return Rest::json(
                    [
                        "msg" => 'erreur'
                    ]
                );
            }
        } else {
            return $this->controller->errorToken(200, "not connected");
        }


        

    }
}


