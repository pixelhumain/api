<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\ActionWithTokenValidation;
use Rest;
use PHDB;
use MongoId;
use Yii;

class GetOneAction extends ActionWithTokenValidation
{
    public function run()
    {
        if(Yii::app()->session["userId"]){
            $id = $_POST["id"];
            $collection = $_POST["collection"];
            $result = PHDB::findOneById($collection, $id);
            return Rest::json($result);
        } else {
            return $this->controller->errorToken(200, "not connected");
        }
    }
}
