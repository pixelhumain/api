<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;
use Rest;
use Person;
use Yii;
use PHDB;
use MongoId;

class MeAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run()
    {
        if(Yii::app()->session["userId"]){
            // enelever du resultat :  pwd, gamification, loginTokens, modifiedByBatch, oceco, userWallet, refreshToken
            $user = PHDB::findOne(Person::COLLECTION, ["_id" => new MongoId(Yii::app()->session["userId"])], ["pwd" => 0, "gamification" => 0, "loginTokens" => 0, "modifiedByBatch" => 0, "oceco" => 0, "userWallet" => 0, "refreshToken" => 0]);
            return Rest::json($user);
        } else {
            return $this->controller->errorToken(200, "not connected");
        }
    }
}
