<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt;

class SignatureInvalidException extends \UnexpectedValueException
{
}
