<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\JWT;
use Rest;
use Person;
use PHDB;
use MongoId;
use Yii;
use DateTimeImmutable;

// Communecter.org fait un appel à 00.re via /api/cocolight/validatecreateuser en envoyant l'exchangeToken et le serverUrl (communecter.org).

class ValidateCreateUserAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {

        $rawData = Yii::$app->getRequest()->getRawBody();
        $data = json_decode($rawData, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return $this->controller->errorToken(400, "Le corps de la requête doit être un JSON valide");
        }

        // Validation des paramètres requis
        $requiredParams = ["exchangeToken", "serverUrl"];
        foreach ($requiredParams as $param) {
            if (!isset($data[$param]) || empty($data[$param])) {
                return $this->controller->errorToken(400, "Le paramètre '$param' est manquant ou vide");
            }
        }

        // Validation du format de 'serverUrl'
        if (!filter_var($data["serverUrl"], FILTER_VALIDATE_URL)) {
            return $this->controller->errorToken(400, "Le 'serverUrl' fourni n'est pas une URL valide");
        }

        if(!isset(Yii::app()->params['jwt']) || !isset(Yii::app()->params['jwt']['domain']) || !isset(Yii::app()->params['jwt']['schema']) || !isset(Yii::app()->params['jwt']['secretKey']) || !isset(Yii::app()->params['jwt']['refreshTokenKey'])){
            return $this->controller->errorToken(500, "Les paramètres de configuration de l'API ne sont pas définis");
        }
        
        // **Extraction du hostname du 'serverUrl'**
        $serverHostname = parse_url($data["serverUrl"], PHP_URL_HOST);
        if (!$serverHostname) {
            return $this->controller->errorToken(400, "Impossible d'extraire le domaine du 'serverUrl'");
        }


        // **Ajout du contrôle de la liste blanche**
        if (isset(Yii::app()->params['jwt']['whiteListServer']) && is_array(Yii::app()->params['jwt']['whiteListServer']) && !empty(Yii::app()->params['jwt']['whiteListServer'])) {
            $whitelistHostnames = array_map(function($url) {
                // Extraire le hostname de l'URL ou utiliser le domaine directement si pas d'URL
                $hostname = parse_url($url, PHP_URL_HOST);
                return $hostname ? $hostname : $url;
            }, Yii::app()->params['jwt']['whiteListServer']);

            if (!in_array($serverHostname, $whitelistHostnames)) {
                return $this->controller->errorToken(403, "Le 'serverUrl' fourni n'est pas autorisé");
            }
        }



        // Appel à Communecter.org pour valider le jeton d'échange
        $url = rtrim($data["serverUrl"], '/') . "/api/cocolight/validateexchangetoken";
        $postData = ["exchangeToken" => $data["exchangeToken"]];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $this->controller->errorToken(500, "Erreur lors de la requête CURL : " . $error_msg);
        }

        curl_close($ch);

        if ($http_code !== 200) {
            return $this->controller->errorToken($http_code, "Erreur lors de l'appel à " . $data["serverUrl"] . ": " . $response);
        }

        $response = json_decode($response, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return $this->controller->errorToken(500, "Réponse JSON invalide reçue de " . $data["serverUrl"]);
        }

        if (isset($response["valid"]) && $response["valid"] === true) {
            $user = $response["user"];

            // Validation des données utilisateur reçues
            $requiredUserFields = ["id", "email", "username", "server"];
            foreach ($requiredUserFields as $field) {
                if (!isset($user[$field]) || empty($user[$field])) {
                    return $this->controller->errorToken(400, "Le champ utilisateur '$field' est manquant ou vide");
                }
            }

            // Optionnel : Validation de l'email
            if (!filter_var($user["email"], FILTER_VALIDATE_EMAIL)) {
                return $this->controller->errorToken(400, "L'email fourni n'est pas valide");
            }

            // Création de l'utilisateur sur 00.re si nécessaire
            $serviceDecrypt = [
                "serviceName" => $user["server"],
                "serviceData" => [
                    "id" => (string)$user["id"],
                    "email" => (string)$user["email"],
                    "username" => (string)$user["username"],
                    "name" => isset($user["name"]) ? (string)$user["name"] : null,
                    "profilImageUrl" => isset($user["profilImageUrl"]) ? (string)$user["profilImageUrl"] : null,
                    "serverCo" => true
                ]
            ];

            $res = Person::serviceOauth($serviceDecrypt);

            if ($res["result"] === true) {
                $res["account"]["serviceName"] = $user["server"];
                // Person::saveUserSessionData($res["account"]);
                // Person::updateLoginHistory((string)$res["account"]["_id"]);

                // Génération des tokens
                $date = new DateTimeImmutable();
                $expire_at = $date->modify('+10 minutes')->getTimestamp();
                $request_data = [
                    'iat' => $date->getTimestamp(),
                    'iss' => Yii::app()->params['jwt']['domain'],
                    'nbf' => $date->getTimestamp(),
                    'exp' => $expire_at,
                    'id' => (string)$res["account"]["_id"],
                ];
                $expire_at_refresh_token = $date->modify('+30 days')->getTimestamp();
                $request_data_refresh_token = [
                    'iat' => $date->getTimestamp(),
                    'iss' => Yii::app()->params['jwt']['domain'],
                    'nbf' => $date->getTimestamp(),
                    'exp' => $expire_at_refresh_token, 
                    'id' => (string)$res["account"]["_id"]
                ];
                $accessToken = JWT::encode(
                    $request_data,
                    Yii::app()->params['jwt']['secretKey'],
                    Yii::app()->params['jwt']['alg']
                );
                $refreshToken = JWT::encode(
                    $request_data_refresh_token,
                    Yii::app()->params['jwt']['refreshTokenKey'],
                    Yii::app()->params['jwt']['alg']
                );

                // je commente car autrement il y a un refresh Token valide donc si on à plusieur device on peut se connecter avec le meme compte
                // PHDB::update('citoyens', ["_id" => new MongoId((string)$res["account"]["_id"])], ['$set' => ["refreshToken" => $refreshToken]]);
                
                $timezone = date_default_timezone_get();
                
                $credentials = [
                    'data' => [
                        'id' => $res['id'],
                        'username' => $res['account']['username'],
                        'email' => $res['account']['email'],
                        'roles' => $res['account']['roles'],
                        'timezone' => $timezone,
                        'lastLoginDate' => $res['account']['lastLoginDate'],
                        'profilImageUrl' => isset($res['account']['profilImageUrl']) ? Yii::app()->params['jwt']['schema'] . "://" . Yii::app()->params['jwt']['domain'] . $res['account']['profilImageUrl'] : '',
                    ],
                    'status' => 201,
                    'message' => $res['msg']
                ];

                return Rest::json([
                    'user' => $credentials['data'],
                    'accessToken' => $accessToken,
                    'refreshToken' => $refreshToken,
                    'status' => $credentials['status']
                ], null, true, 'application/json; charset=utf-8');

            } else {
                return $this->controller->errorToken(500, "Erreur lors de la création de l'utilisateur : " . $res['msg']);
            }

        } else {
            return $this->controller->errorToken(401, "Le jeton d'échange est invalide ou expiré");
        }

    }
}