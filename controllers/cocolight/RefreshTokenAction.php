<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\JWT;
use PixelHumain\PixelHumain\modules\api\controllers\cocolight\jwt\Key;
use Rest;
use Person;
use DateTimeImmutable;
use Yii;
use PHDB;
use MongoId;

class RefreshTokenAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {

        $data = json_decode(Yii::$app->getRequest()->getRawBody(), true);
        if(isset($data["refreshToken"])){
            try {
            $decoded = JWT::decode($data["refreshToken"],new Key(Yii::app()->params['jwt']['refreshTokenKey'], Yii::app()->params['jwt']['alg']));
            if($decoded){
                $user = PHDB::findOne(Person::COLLECTION, ["_id" => new MongoId($decoded->id)]);
                if(!$user) {
                    return $this->controller->errorToken(401, "User not found");
                }
                $date = new DateTimeImmutable();
                $expire_at = $date->modify('+10 minutes')->getTimestamp();
                $request_data = [
                    'iat' => $date->getTimestamp(),
                    'iss' => Yii::app()->params['jwt']['domain'],
                    'nbf' => $date->getTimestamp(),
                    'exp' => $expire_at,
                    'id' => $decoded->id,
                ];
                $accessToken = JWT::encode(
                    $request_data,
                    Yii::app()->params['jwt']['secretKey'],
                    Yii::app()->params['jwt']['alg']
                );

                $expire_at_refresh_token = $date->modify('+30 days')->getTimestamp();
                $request_data_refresh_token = [
                    'iat' => $date->getTimestamp(),
                    'iss' =>  Yii::app()->params['jwt']['domain'],
                    'nbf' => $date->getTimestamp(),
                    'exp' => $expire_at_refresh_token, 
                    'id' => $decoded->id
                ];
                $refreshToken = JWT::encode(
                    $request_data_refresh_token,
                    Yii::app()->params['jwt']['refreshTokenKey'],
                    Yii::app()->params['jwt']['alg']
                );

                // je commente car autrement il y a un refresh Token valide donc si on à plusieur device on peut se connecter avec le meme compte
                // PHDB::update('citoyens', array("_id" => new MongoId($decoded->id)), array('$set' => array("refreshToken"=> $refreshToken )));

                return Rest::json(["token" => $accessToken, "refreshToken" => $refreshToken]);
            }
            } catch (\Exception $e) {
                return $this->controller->errorToken(401, "Token is invalid or expired");
            }
        } 
    }
}
