<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\cocolight;

use PixelHumain\PixelHumain\modules\api\controllers\cocolight\ActionWithTokenValidation;
use Rest;
use PHDB;
use MongoId;
use Yii;
use Poi;

class PoiAction extends ActionWithTokenValidation
{
    public function run()
    {

        if(Yii::app()->session["userId"]){
            $id = $_POST["id"];
            $result = PHDB::find(Poi::COLLECTION, array("parent.$id" => array('$exists' => true)));
            return Rest::json(
                [
                    "count" => count($result),
                    "result" => $result
                ]
            );
        } else {
            return $this->controller->errorToken(200, "not connected");
        }
        

    }
}

