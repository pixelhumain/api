<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Exception;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use Rest;
use Yii;
use PHDB;

class GetGroupAction extends \PixelHumain\PixelHumain\components\Action{
	public function __construct($id, $controller, $config){
		parent::__construct($id, $controller, $config);
	//	$this->getController()->setDefaultUrl(Utils::createUrl("#@".Yii::$app->request->get("g")));
	}

	public function run($g) {
		try{
			$group = ActivitypubActor::getCoGroupAsActor($g);
            if($group){
                return json_encode($group->toArray());
            } else {
                throw new Exception("Organization not found", 404);
            }
        } catch (Exception $e) {
            return json_encode(["error" => $e->getMessage()]);
        }
	}
}
