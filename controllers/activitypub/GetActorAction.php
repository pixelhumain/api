<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Exception;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use Rest;
use Yii;
use yii\web\Response;
class GetActorAction extends \PixelHumain\PixelHumain\components\Action{
	public function __construct($id, $controller, $config){
		parent::__construct($id, $controller, $config);
	//	$this->getController()->setDefaultUrl(Utils::createUrl("#@".Yii::$app->request->get("u")));
	}

	public function run($u = null) {
        try {
            $person = ActivitypubActor::getCoPersonAsActor($u);
            if ($person) {
                return json_encode($person->toArray());
            } else {
                throw new Exception("User not found", 404);
            }
        } catch (Exception $e) {
            return json_encode(["error" => $e->getMessage()]);
        }
	}
}
