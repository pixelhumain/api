<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Exception;
use PH;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use Rest;
use Yii;
use PHDB;

class GetProjectAction extends \PixelHumain\PixelHumain\components\Action{
    public function __construct($id, $controller, $config){
        parent::__construct($id, $controller, $config);
        $this->getController()->setDefaultUrl(Utils::createUrl("#@".Yii::$app->request->get("p")));
    }

    public function run($p) {
        try{
            $project = ActivitypubActor::getCoProjectAsObject($p);
            return Rest::json($project->toArray(), null, true, 'application/activity+json; charset=utf-8');
        }catch(Exception $e){
           
            return Rest::json(["error"=>$e->getMessage()], null, true, 'application/activity+json; charset=utf-8');
        }
    }
}
