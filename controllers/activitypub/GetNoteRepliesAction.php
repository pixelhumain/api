<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Rest;

class GetNoteRepliesAction extends \PixelHumain\PixelHumain\components\Action{
	public function run() {
		if(!isset($_POST["noteId"]))
            return Rest::json(["error"=>true, "msg"=>"Missing noteId parameter"]);
        $index = isset($_POST["index"]) ? $_POST["index"]:null;
        $replies = ActivitypubObject::getRepliesOfNote($_POST["noteId"], $index);
        return Rest::json($this->convertReplies($replies));
	}

    private function convertReplies($replies){
        $convertedReplies = [];
        foreach($replies as $replie){
            $actor = Type::createFromAnyValue($replie->get("attributedTo"));

            $note = [
                "id" => $replie->get("id"),
                "content" => $replie->get("content"),
                "attachments" => []
            ];
    
            if(is_array($replie->get("attachment"))){
                foreach($replie->get("attachment") as $attachment){
                    $isValidImageType = in_array($attachment->get("mediaType"), ['image/png', 'image/jpeg','image/jpg']);
                    $isValidUrl = filter_var($attachment->get("url"), FILTER_VALIDATE_URL);
    
                    if($isValidImageType && $isValidUrl)
                        $note["attachments"][] = $attachment->get("url");
                }
            }

            $note["author"] = [
                "id" => $actor->get("id"),
                "name" => $actor->get("name"),
                "address" => ActivitypubTranslator::getActorAdress($actor),
            ];
            if($actor->get("icon") && $actor->get("icon")->get("type") == "Image"){
                $note["author"]["avatar"] = $actor->get("icon")->get("url");
            }

            $convertedReplies[] = $note;
        }
        return $convertedReplies;
    }
}
