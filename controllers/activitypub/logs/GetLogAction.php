<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\logs;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\logs\LogParser;
use Rest;
use Yii;
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("HTTP/1.1 200 OK");
    exit;
}

class GetLogAction extends \PixelHumain\PixelHumain\components\Action{
    public function run() {

         /**
         * Met ça dans params config
          *
          *     "log_directory" =>  dirname(__DIR__).DIRECTORY_SEPARATOR.'runtime',
          *
         */
           $logDirectory = Yii::app()->params['log_directory'];
          $list = LogParser::getApplicationLog($logDirectory);
           $log = LogParser::openLog($list);
           $output = LogParser::getLogOutput($log);
           return Rest::json(["rows" => $output,'count' => count($output)]);
    }
}
