<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Exception;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use Rest;
use Yii;

class InboxAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($u=null) {
		try{
			//TODO: check the user $u
           	$data = json_decode(Yii::$app->getRequest()->getRawBody(), true); 
           if(isset($data["signature"]))
				unset($data["signature"]);

			if(isset($data["type"])){
				$activity =  Type::create($data["type"], $data);
                if($activity instanceof Activity){
					Handler::handle($activity);	
				}
			}
			
		}catch(Exception $e){
			return Rest::json(["error"=> $e->getMessage()]);
			
		}
	}
}
