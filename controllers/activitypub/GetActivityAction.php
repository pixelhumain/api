<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Exception;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use Rest;

class GetActivityAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($id) {
        try{
            $activity = ActivitypubActivity::getByUUID($id);
            if(!$activity)
                throw new Exception("Activity not found");
            return Rest::json($activity->toArray(), null, true, 'application/activity+json; charset=utf-8');
        }catch(Exception $e){
            return Rest::json(["error" => $e->getMessage()]);
        }
	}
}
