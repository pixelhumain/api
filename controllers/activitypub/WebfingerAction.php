<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Yii;
use Exception;
use Person;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Webfinger;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use Project;
use Rest;
use yii\web\Response;

class WebfingerAction extends \PixelHumain\PixelHumain\components\Action {
    public function run()
    {
        $queryParams = $this->extractQueryParams($_SERVER['QUERY_STRING']);

        if (!isset($_GET["resource"])) {
            $this->setHttpStatus(400);
            return $this->jsonResponse(["error" => "Resource parameter is missing"], "application/jrd+json");
        }

        try {
            if (!str_starts_with($_GET["resource"], "acct:")) {
                $this->setHttpStatus(400);
                return $this->jsonResponse(["error" => "Resource must start with 'acct:'"]);
            }

            $resource = $_GET["resource"];
            $resource = str_replace("acct:", "", $resource);

            if (!$this->isValidResourceFormat($resource)) {
                $this->setHttpStatus(404);
                return $this->jsonResponse(["error" => "Invalid resource format. Must be 'acct:username@host'"], "application/jrd+json");
            }

            $resourceParts = Webfinger::parseResource($resource);

            if ($resourceParts["host"] !== Config::HOST()) {
                throw new Exception("Host not valid");
            }


            $group = PHDB::findOne(Organization::COLLECTION, [
                "slug" => new \MongoRegex('/^'.$resourceParts["user"].'$/i')
            ]);

            $user = PHDB::findOne(Person::COLLECTION, [
                '$or' => [
                    ["username" => new \MongoRegex('/^'.$resourceParts["user"].'$/i')],
                    ["slug" => new \MongoRegex('/^'.$resourceParts["user"].'$/i')]
                ]
            ]);

            // Vérification du type pour le groupe
            $isValidGroup = $group && isset($group['collection']) && $group['collection'] == 'organizations';

            // Si aucune entité n'est trouvée
            if (!$user && !$group) {
                $this->setHttpStatus(404);
                return $this->jsonResponse(["error" => "Resource not found"], "application/jrd+json");
            }

            $webfinger = new Webfinger();
            $resource = (!str_starts_with($resource, "acct:")) ? "acct:" . $resource : $resource;
            $webfinger->setSubject($resource);

            $aliases = [];
            $links = [];

            // Si c'est un groupe valide de type organizations, on le priorise
            if ($isValidGroup) {
                $this->addGroupLinks($group, $aliases, $links, $queryParams);
            }
            // Sinon on utilise l'utilisateur si disponible
            else if ($user) {
                $this->addUserLinks($user, $aliases, $links, $queryParams);
            }

            $webfinger->setAliases($aliases);
            $webfinger->setLinks($links);

            $this->setHttpStatus(200);
            return Rest::json($webfinger->toArray());

        } catch (Exception $e) {
            $statusCode = $e->getCode() ?: 500;
            $this->setHttpStatus($statusCode);
            return $this->jsonResponse(["error" => $e->getMessage()]);
        }
    }

    private function addGroupLinks($group, &$aliases, &$links, $queryParams)
    {
        $groupUrl = Utils::createUrl("api/activitypub/groups/g/".$group["slug"]);
        if ($this->checkUrlExists($groupUrl)) {
            $aliases[] = $groupUrl;
            $links[] = [
                "rel" => "self",
                "type" => "application/activity+json",
                "href" => $groupUrl
            ];

            $this->addAdditionalLinks($groupUrl, $links, $queryParams);
        }
    }

    private function addUserLinks($user, &$aliases, &$links, $queryParams)
    {
        $userUrl = Utils::createUrl("api/activitypub/users/u/".$user["username"]);
        if ($this->checkUrlExists($userUrl)) {
            $aliases[] = $userUrl;
            $links[] = [
                "rel" => "self",
                "type" => "application/activity+json",
                "href" => $userUrl
            ];

            $this->addAdditionalLinks($userUrl, $links, $queryParams);
        }
    }

    private function addAdditionalLinks($baseUrl, &$links, $queryParams)
    {
        if (isset($queryParams["rel"])) {
            $rels = is_array($queryParams["rel"]) ? $queryParams["rel"] : [$queryParams["rel"]];
            foreach ($rels as $rel) {
                $links[] = [
                    "rel" => $rel,
                    "href" => $baseUrl
                ];
            }
        }
    }

    private function setHttpStatus($statusCode)
    {
        Yii::$app->response->statusCode = $statusCode;
    }
    private function jsonResponse($data, $contentType)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->headers->set('Content-Type', $contentType);
        return $data;
    }
    private function extractQueryParams($queryString)
    {
        $regex = '/(?:\?|&)([^=&]+)=([^&]*)/';
        $params = [];

        preg_match_all($regex, $queryString, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            $key = urldecode($match[1]);
            $value = urldecode($match[2]);

            if (array_key_exists($key, $params)) {
                if (!is_array($params[$key])) {
                    $params[$key] = [$params[$key]];
                }
                $params[$key][] = $value;
            } else {
                $params[$key] = $value;
            }
        }

        return $params;
    }
    private function checkUrlExists($url)
    {
        $headers = @get_headers($url);
        return $headers && strpos($headers[0], '200') !== false;
    }
    private function isValidResourceFormat($resource)
    {
        $pattern = '/^[\w\-\.]+@[\w\.\-]+$/';
        return preg_match($pattern, $resource);
    }
}
