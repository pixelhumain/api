<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection;

use Exception;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Helper;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\layers\server\FollowHandler as ServerFollowHandler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\layers\server\http\Request;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Create;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Follow;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\object\Note;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use Rest;
use Translate;
use TranslateActivityStream;
use Yii;

class OutboxAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($u){
        /* $request = Yii::$app->request;
        $page = intval($request->get("page"));

        //check the username
        if(!$user = PHDB::findOne(Person::COLLECTION, ["username"=>$u]))
            return Rest::json(["error" => "Record not found"]);

        //retrieve user id
        $userId = $user["_id"]->{'$id'};

        //get the total of outbox items
        $totalItems = PHDB::count(Activitypub::COLLECTION, ["collection"=>Activitypub::OUTBOX_COLLECTON, "owners"=>['$in'=>[$userId]]]);
        //calculate the number of pages based on the total number of items
        $totalPages = Activitypub::calculateNumberOfPage($totalItems);

        //FIRST CASE: if the number of pages is not specified, we just return the information about the outbox without mentioning the items
        if($page==0)
            return Rest::json(Translate::convert([
                [
                    "id" => "/outbox/u/".$u,
                    "totalItems" => $totalItems,
                    "first" => "/outbox/u/".$u."?page=1",
                    "last" => "/outbox/u/".$u."?page=".$totalPages
                ]
            ], TranslateActivityStream::$dataBinding_orderedcollection)[0]);
        
        //SECOND CASE: collect the items and return it
        //retrieve the user's outbox
        $items = Activitypub::findByOwnerAndType($userId, Activitypub::OUTBOX_COLLECTON, $page);
        //Prepare to give them for the formatting of data in return
        $translateData = [
            "items" => $items,
            "endpoint" => "/outbox/u/".$u."?page=".$page,
            "partOf" => "/outbox/u/".$u,
            "prev" => "/outbox/u/".$u.($page-1>0?"?page=".$page-1:"")
        ];
        //add the next field if there is still one
        if($page+1 <= $totalPages)
            $translateData["next"] = "/outbox/u/".$u."?page=".($page+1);
        //translate data
        $outbox = Translate::convert([$translateData],TranslateActivityStream::$dataBinding_orderedcollectionpage)[0];
        //check if orderedItems does not exist and add it
        if(!isset($outbox["orderedItems"]))
            $outbox["orderedItems"] = [];

        return Rest::json($outbox); */
        /* $note = new Note();
        $note->setName("A Word of Warning");
        $note->setContent("Looks like it is going to rain today");

        $activity = new Create();
        $activity->set_context("https://www.w3.org/ns/activitystreams");
        $activity->setSummary("Sally created a note");
        $activity->setObject($note);

        var_dump($activity->toArray()); */

        // Then, after your curl_exec call:
        /* try{
            $followActivity = new Follow();
            $followActivity->set_context("https://www.w3.org/ns/activitystreams");
            $followActivity->setId("https://mastodon.social/f1ce017e-3a66-444d-8e45-8469dfb77039");
            $followActivity->setActor("https://mastodon.social/users/yorre");
            $followActivity->setObject("https://7e46-102-16-42-24.ngrok.io/api/activitypub/person/u/yorre");

            $followHandler = new ServerFollowHandler($followActivity);
            $followHandler->handle();
        }catch(Exception $e){
            return Rest::json(["error"=>$e->getMessage()]);
        } */
    }
}