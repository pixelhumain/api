<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection;

use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\OrderedCollection;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\OrderedCollectionPage;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Project;
use Rest;
use Translate;
use TranslateActivityStream;
use Yii;

class GetSuggestionTagsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $tags  = ActivitypubLink::getAllTags();

        return Rest::json($tags, null, true, 'application/activity+json; charset=utf-8');
    }
}
