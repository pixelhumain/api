<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection;

use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\OrderedCollection;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\OrderedCollectionPage;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Rest;
use Translate;
use TranslateActivityStream;
use Yii;

class FollowingAction extends \PixelHumain\PixelHumain\components\Action
{
    private $pageSize = 2;
    public function run($u)
    {
        $request = Yii::$app->request;
        $page = $request->get("page");
        if (!isset($page)) {
          $page = 1;
        } else {
          $page = intval($request->get("page"));
        }
        $follows = [];
        $orderedCollection = new OrderedCollection();
        $orderedCollection->setId(Utils::createUrl("api/activitypub/following/u/" . $u));
        if ($user = PHDB::findOne(Person::COLLECTION, ["username" => $u])) {
            $actor = ActivitypubTranslator::coPersonToActor($user);
            $orderedCollection->setAttributedTo($actor->get('id'));
            if (isset($user["links"]["activitypub"]["follows"]) && count($user["links"]["activitypub"]["follows"]) > 0) {
                $totalPages = ceil(count($user["links"]["activitypub"]["follows"]) / $this->pageSize);
                $orderedCollection->set('totalItems', count($user["links"]["activitypub"]["follows"]));
                foreach ($user["links"]["activitypub"]["follows"] as $ref => $link) {
                    $follows[] = $link['invitorId'];
                }
                $data = array(
                    "attributedTo" => $actor->get('id'),
                    "id" => Utils::createUrl("api/activitypub/following/u/" . $u . "?page=" . $page),
                    "next" => Utils::createUrl("api/activitypub/following/u/" . $u . "?page=" . ($page + 1)),
                    "orderedItems" => self::paginate($follows, $page),
                    "partOf" => Utils::createUrl("api/activitypub/following/u/" . $u)
                );
                if($page>1 && $page + 1 > $totalPages){
                    unset($data['next']);

                    $data["last"] = Utils::createUrl("api/activitypub/following/u/" . $u . "?page=" . $totalPages);
                }

                $orderedCollectionPage = Type::create("OrderedCollectionPage", $data);
                $orderedCollection->setFirst($orderedCollectionPage);
            } else {
                return Rest::json([]);
            }
        } else {
            return Rest::json([]);
        }
        return Rest::json($orderedCollection->toArray(), null, true, 'application/activity+json; charset=utf-8');
    }
    private function paginate($array, $page)
    {
        $start = ($page - 1) * $this->pageSize;
        $end = $start + $this->pageSize;
        $elementsPage = array_slice($array, $start, $end);
        return $elementsPage;
    }
}
