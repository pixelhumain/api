<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\project;

use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\OrderedCollection;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\OrderedCollectionPage;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Project;
use Rest;
use Translate;
use TranslateActivityStream;
use Yii;

class ContributorsAction extends \PixelHumain\PixelHumain\components\Action
{
    private $pageSize = 2;
    public function run($p)
    {
        $request = Yii::$app->request;
        $page = $request->get("page");
        if (!isset($page)) {
          $page = 1;
        } else {
          $page = intval($request->get("page"));
        }
        $contributors = [];
        $orderedCollection = new OrderedCollection();
        $orderedCollection->setId(Utils::createUrl("api/activitypub/contributors/p/" . $p));
        if ($project = PHDB::findOne(Project::COLLECTION, ["slug" => $p])) {
            $actor = ActivitypubTranslator::coPersonToActor($project);
            $orderedCollection->setAttributedTo($actor->get('id'));
            if (isset($project["links"]["activitypub"]["contributors"]) && count($project["links"]["activitypub"]["contributors"]) > 0) {
                $totalPages = ceil(count($project["links"]["activitypub"]["contributors"]) / $this->pageSize);
                $orderedCollection->set('totalItems', count($project["links"]["activitypub"]["contributors"]));
                foreach ($project["links"]["activitypub"]["contributors"] as $ref => $link) {
                    $contributors[] = $link['invitorId'];
                }
                $data = array(
                    "attributedTo" => $actor->get('id'),
                    "id" => Utils::createUrl("api/activitypub/contributors/p/" . $p . "?page=" . $page),
                    "next" => Utils::createUrl("api/activitypub/contributors/p/" . $p . "?page=" . ($page + 1)),
                    "orderedItems" => self::paginate($contributors, $page),
                    "partOf" => Utils::createUrl("api/activitypub/contributors/p/" . $p)
                );
                if($page>1 && $page + 1 > $totalPages){
                    unset($data['next']);

                    $data["last"] = Utils::createUrl("api/activitypub/contributors/p/" . $p . "?page=" . $totalPages);
                }

                $orderedCollectionPage = Type::create("OrderedCollectionPage", $data);
                $orderedCollection->setFirst($orderedCollectionPage);
            } else {
                return Rest::json([]);
            }
        } else {
            return Rest::json([]);
        }
        return Rest::json($orderedCollection->toArray(), null, true, 'application/activity+json; charset=utf-8');
    }
    private function paginate($array, $page)
    {
        $start = ($page - 1) * $this->pageSize;
        $end = $start + $this->pageSize;
        $elementsPage = array_slice($array, $start, $end);
        return $elementsPage;
    }
}
