<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\project;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use Rest;
use PHDB;
use Person;
use Exception;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Preference;

class ManageRolesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        if (!isset($_SESSION["userId"]))
            return Rest::json(["error" => true, "msg" => "You must authenticate to perform this operation."]);
        if (!isset($_POST["roles"]) && !isset($_POST["objectId"]) && !isset($_POST["targetId"]))
            return Rest::json(["error" => true, "msg" => "Missing parameters."]);
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);
        if (Preference::isActivitypubActivate($user["preferences"]))
            try {
                $params = array("connectType" => "withRules", "objectId" => $_POST["objectId"],  "targetId" => $_POST["targetId"], "rules" =>  explode(",", $_POST["roles"]));
                Handler::handle(ActivitypubActivityCreator::createOfferProjectActivity($params));
                return Rest::json([
                    "result" => true,
                    "msg" => "",
                    "roles" =>  explode(",", $_POST["roles"])
                ]);
            } catch (Exception $e) {
                return Rest::json(["error" => $e->getMessage()]);
            }
    }
}
