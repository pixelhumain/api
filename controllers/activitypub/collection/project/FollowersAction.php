<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\project;

use Person;
use PHDB;
use Rest;
use Translate;
use TranslateActivityStream;
use Yii;

class FollowersAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($u){
        /* $request = Yii::$app->request;
        $page = intval($request->get("page"));

        if(!$user = PHDB::findOne(Person::COLLECTION, ["username"=>$u]))
            return Rest::json(["error" => "Record not found"]);

        $followers = Activitypub::getLinks("followers", $user["_id"]->{'$id'});
        $totalPages = Activitypub::calculateNumberOfPage(sizeof($followers));
        
        if($page==0)
            return Rest::json(Translate::convert([
                [
                    "id" => "/followers/u/".$u,
                    "totalItems" => sizeof($followers),
                    "first" => "/followers/u/".$u."?page=1",
                    "last" => "/followers/u/".$u."?page=".$totalPages
                ]
            ], TranslateActivityStream::$dataBinding_orderedcollection)[0]);

        $output = Translate::convert([[
            "items" => $followers,
            "endpoint" => "/followers/u/".$u."?page=".$page,
            "partOf" => "/followers/u/".$u,
            "prev" => "/followers/u/".$u.($page-1>0?"?page=".$page-1:"")
        ]],TranslateActivityStream::$dataBinding_orderedcollectionpage)[0];

        if(!isset($output["orderedItems"]))
            $output["orderedItems"] = [];
            
        return Rest::json($output); */
    }

    
}