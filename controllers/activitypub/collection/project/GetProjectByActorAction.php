<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\project;

use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\OrderedCollection;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Rest;
use Yii;

class GetProjectByActorAction extends \PixelHumain\PixelHumain\components\Action
{
    private $pageSize = 2;
    public function run($u)
    {
        $request = Yii::$app->request;
        $page = $request->get("page");
        if (!isset($page)) {
          $page = 1;
        } else {
          $page = intval($request->get("page"));
        }
        $allProjects  = [];
        $orderedCollection = new OrderedCollection();
        $orderedCollection->setId(Utils::createUrl("api/activitypub/projects/u/" . $u));
        if ($actor = ActivitypubActor::getCoPersonAsActor($u)) {
            $projects = ActivitypubObject::getProjects($actor->get('id'));
            $orderedCollection->set('totalItems', count($projects));
            $totalPages = ceil(count($projects) / $this->pageSize);
            foreach ($projects as $key => $prj) {
               $allProjects[] = $prj['object'];
            }
            $data = array(
                "attributedTo" => $actor->get('id'),
                "id" => Utils::createUrl("api/activitypub/projects/u/" . $u . "?page=" . $page),
                "next" => Utils::createUrl("api/activitypub/projects/u/" . $u . "?page=" . ($page + 1)),
                "orderedItems" => self::paginate($allProjects, $page),
                "partOf" => Utils::createUrl("api/activitypub/projects/u/" . $u)
            );
            if($page>1 && $page + 1 > $totalPages){
                unset($data['next']);
                $data["last"] = Utils::createUrl("api/activitypub/projects/u/" . $u . "?page=" . $totalPages);
            }

            $orderedCollectionPage = Type::create("OrderedCollectionPage", $data);
            $orderedCollection->setFirst($orderedCollectionPage);
        } else {
            return Rest::json([]);
        }
        return Rest::json($orderedCollection->toArray(), null, true, 'application/activity+json; charset=utf-8');
    }
    private function paginate($array, $page)
    {
        $start = ($page - 1) * $this->pageSize;
        $end = $start + $this->pageSize;
        $elementsPage = array_slice($array, $start, $end);
        return $elementsPage;
    }
}
