<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\collection\event;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use Rest;
use PHDB;
use Person;
use Preference;
class CreateEventAction extends \PixelHumain\PixelHumain\components\Action{
	public function run() {
        if(!isset($_SESSION["userId"]))
            return Rest::json(["error"=>true, "msg"=>"You must authenticate to perform this operation."]);
        $user = PHDB::findOneById(Person::COLLECTION, $_SESSION["userId"]);

        if(!isset($_POST["payload"]))
            return Rest::json(["error"=>true, "msg"=>"Missing parameters."]);
        if(Preference::isActivitypubActivate($user["preferences"]))
            try{
                Handler::handle(ActivitypubActivityCreator::createEventActivity($_POST['payload']));
            }catch(Exception $e){
                return Rest::json(["error"=>$e->getMessage()]);
            }
	}
}
