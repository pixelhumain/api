<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Exception;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubTranslator;
use Rest;
use Yii;

class SearchAction extends \PixelHumain\PixelHumain\components\Action{
	public function run(){
        try{
            $user = PHDB::findOneById(Person::COLLECTION, Yii::app()->session["userId"]);
            $actor = ActivitypubActor::searchActorByRessourceAddress($_GET["address"]);

            $link = [];

            $actorRef = ActivitypubTranslator::getActorAdress($actor, true);

            if(isset($user["links"]["activitypub"]["follows"][$actorRef]))
                $link["follows"] = $user["links"]["activitypub"]["follows"][$actorRef];
            if(isset($user["links"]["activitypub"]["followers"][$actorRef]))
                $link["followers"] = $user["links"]["activitypub"]["followers"][$actorRef];

            return Rest::json([
               "error" => false,
               "actor" => [
                  "link" => $link,
                  "data" => $actor->toArray()
               ]
            ]);
        }catch(Exception $e){
            return Rest::json(["error"=>true, "message"=>$e->getMessage()]);
        }
    }
}
