<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use ActivityStr;
use DateTime;
use Exception;
use MongoId;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Accept;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Follow;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Reject;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\activity\Undo;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubLink;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\notification\NotificationTransformer;
use Rest;
use Yii;

class LinkAction extends \PixelHumain\PixelHumain\components\Action
{
    private $actor;
    private $subject;
    private  $actorObject;
    private  $actorId;
    private $payload;

    public function run()
    {
        try {
            //verifier si l'utilisateur a bien logguer
            $actorParams = $_POST["actor"];
            if ($actorParams["type"] == "group") {
                if (!isset($actorParams["id"]))
                    throw new Exception("Group not found");
            } else if ($actorParams["type"] == "project") {
                if (!isset($actorParams["id"]))
                    throw new Exception("Project not found");
            } else {
                if (!isset($_SESSION["userId"]))
                    throw new Exception("You must authenticate to perform this operation");
            }
            if (!isset($actorParams['name'])) {
                $actorParams["name"] = $_SESSION["user"]["username"];
            }
            $this->actorObject = $actorParams;


            $this->actor = $actorParams["type"] == "group" ? ActivitypubActor::getCoGroupAsActor($actorParams["name"]): ActivitypubActor::getCoPersonAsActor($_SESSION["user"]["username"]);

            if ($actorParams["type"] === "group") {
                $this->actorId = $actorParams["id"];
            } else if ($actorParams["type"] === "project") {
                $this->actorId = $actorParams["id"];
            } else {
                $this->actorId = $_SESSION["userId"];
            }
            if($_POST['action'] != 'add-tag'){
                if (!is_array($_POST["payload"])) {
                    $this->subject = Type::createFromAnyValue($_POST["payload"]);
                    $this->payload = array();
                } else {
                    $this->subject = Type::createFromAnyValue($_POST["payload"]['objectId']);
                    $this->payload = $_POST["payload"];
                }
            }else{
                $this->subject = null;
                $this->payload = $_POST["payload"] ?? [];
            }

            $action = $_POST["action"];
            switch ($action) {
                case "follow":
                    $this->handleFollow();
                    break;
                case "accept_follower":
                    $this->handleAcceptFollower();
                    break;
                case "undo_accept":
                    $this->handleRemoveFollower();
                    break;
                case "reject_follower":
                    $this->rejectFollower();
                    break;
                case "undo_follow":
                    $this->undoFollow();
                    break;
                // simple contributor request
                case "contributor":
                    $this->handleContribute("contributor");
                    break;
                // request admin contribute
                case "contribute_as_admin":
                    $this->handleContribute("admin");
                    break;
                // accept contribute request
                case "isAdminPending":
                case "toBeValidated":
                    $this->handleAcceptContribute();
                    break;
                 // accept contribute as admin request
                case 'addAsAdmin':
                    $this->handleAddAsAdminInProject();
                    break;
                // invite to be contributor,admin,ect ..
                case "invite":
                    $this->handleCreateInvitationOnProject();
                    break;
                // delete contribution ..
                case "delete_contribution":
                    $this->handleDeleteContribution();

                    break;
                case "acceptInvitation":
                    $this->handleAcceptInvitationOnProject();
                    break;
                case "rejectInvitation":
                    break;
                case "accept_invitation":
                    $this->handleAcceptInvitation();
                    break;
                case "reject_invitation":
                    $this->rejectInvitation();
                    break;
                case "undo_invitation":
                    $this->undoInvitation();
                    break;
                case "follow_project":
                    $this->handleFollowProject();
                    break;
                case "unfollow_project":
                    $this->handleLeaveFollow();
                    break;
                case "update_project":
                    $this->updateProject();
                    break;
                case "leave_contribution":
                    $this->handleLeaveContribution();
                    break;

                // special link
                case 'add-tag':
                    $this->handleAddTag();
                    break;
            }
            return Rest::json(["error" => false, "result"=> array('message' => 'ok'), "msg" => 'Opération réussie avec succès.']);
        } catch (Exception $e) {
            return Rest::json(["error" => true, "message" => $e->getMessage()]);
        }
    }

    private function handleAcceptFollower()
    {
        $activity = ActivitypubActivity::getLinkActivity("followers", $this->actorId, $this->subject, $this->actorObject['type']);

        $acceptActivity = new Accept();
        $acceptActivity->setObject($activity->get("id"));
        $acceptActivity->setActor($this->actor->get("id"));
        $summary = Yii::t(
            "activitypub",
            "{who} has accepted your follow request",
            array(
                "{who}" => $this->actor->get("name")
            )
        );

        $acceptActivity->setSummary($summary);
        Handler::handle($acceptActivity, $this->actorObject);
    }

    private function handleFollow()
    {

        $followActivity = new Follow();
        $followActivity->setObject($this->subject->get("id"));
        $followActivity->setActor($this->actor->get("id"));
        // notification
        $summary = Yii::t(
            "activitypub",
            "{who} follow you",
            array(
                "{who}" => $this->actor->get("name")
            )
        );
        $followActivity->setSummary($summary);
        Handler::handle($followActivity, $this->actorObject);
    }

    private function handleRemoveFollower()
    {
        $followActivity = ActivitypubActivity::getLinkActivity("followers", $this->actorId, $this->subject,  $this->actorObject['type']);

        //get accept activity
        $acceptActivity = PHDB::findOne(ActivitypubActivity::COLLECTION, [
            "activity.type" => "Accept",
            "activity.object" => $followActivity->get("id")
        ]);
        $acceptActivity = Type::createFromAnyValue($acceptActivity["activity"]);

        $undoAcceptActivity = new Undo();
        $undoAcceptActivity->set("actor", $this->actor->get("id"));
        $undoAcceptActivity->set("object", $acceptActivity);
        $undoAcceptActivity->set("target", $followActivity->get("actor"));
         $summary = Yii::t(
            "activitypub",
            "{who} has remove his follow",
            array(
                "{who}" => $this->actor->get("name")
            )
        );


        $undoAcceptActivity->setSummary($summary);
        Handler::handle($undoAcceptActivity, $this->actorObject);
    }

    private function rejectFollower()
    {
        $followActivity = ActivitypubActivity::getLinkActivity("followers", $this->actorId, $this->subject, $this->actorObject['type']);

        $rejectFollowActivity = new Reject();
        $rejectFollowActivity->set("actor", $this->actor->get("id"));
        $rejectFollowActivity->set("object", $followActivity);
         $summary = Yii::t(
            "activitypub",
            "{who} has reject his follow",
            array(
                "{who}" => $this->actor->get("name")
            )
        );

        $rejectFollowActivity->setSummary($summary);
        Handler::handle($rejectFollowActivity, $this->actorObject);
    }

    private function undoFollow()
    {
        $followActivity = ActivitypubActivity::getLinkActivity("follows", $this->actorId, $this->subject,  $this->actorObject['type']);
        $undoFollowActivity = new Undo();
        $undoFollowActivity->set("actor", $this->actor->get("id"));
        $undoFollowActivity->set("object", $followActivity);
         $summary = Yii::t(
            "activitypub",
            "{who} has undo his follow",
            array(
                "{who}" => $this->actor->get("name")
            )
        );

        $undoFollowActivity->setSummary($summary);
        Handler::handle($undoFollowActivity, $this->actorObject);
    }

    public function handleAcceptInvitation()
    {
        $activity = ActivitypubActivity::getLinkActivity("followers", $this->actorId, $this->subject, $this->actorObject['type']);
        $acceptActivity = new Accept();
        $acceptActivity->setObject($activity->get("id"));
        $acceptActivity->setActor($this->actor->get("id"));
        $summary = Yii::t(
            "activitypub",
            "{who} has accepted your invitation",
            array(
                "{who}" => $this->actor->get("name")
            )
        );
        $acceptActivity->setSummary($summary);
        Handler::handle($acceptActivity, $this->actorObject);
    }
    public function rejectInvitation(): void
    {
        $activity = ActivitypubActivity::getLinkActivity("followers", $this->actorId, $this->subject, $this->actorObject['type']);

        $rejectInviteActivity = new Reject();
        $rejectInviteActivity->set("actor", $this->actor->get("id"));
        $rejectInviteActivity->set("object", $activity);
        $summary = Yii::t(
            "activitypub",
            "{who} has refused your follow request",
            array(
                "{who}" => $this->actor->get("name")
            )
        );
        $rejectInviteActivity->setSummary($summary);
        Handler::handle($rejectInviteActivity, $this->actorObject);
    }

    /**
     * @throws Exception
     */
    private function handleContribute($type): void
    {
        $activity = ActivitypubActivityCreator::createContributionActivity($this->subject, $type);
        Handler::handle($activity, $this->actorObject);
    }
    private function handleAcceptContribute(): void
    {
        if(isset($this->payload['targetId'])){
            $activity = ActivitypubActivityCreator::acceptContributionActivity($this->payload['targetId'],$this->payload['objectId']);
            Handler::handle($activity, $this->actorObject);
        }else{
            return;
        }

    }
    private function updateProject(): void
    {
       $activity = ActivitypubActivityCreator::updateProjectActivity($this->actor, $this->subject, $this->payload);
       Handler::handle($activity, $this->actorObject);
    }

    public function handleCreateInvitationOnProject(): void
    {
        $invitationActivity = ActivitypubActivityCreator::createInvitationProjectActivity($this->payload);
        Handler::handle($invitationActivity, $this->actorObject);
    }
    public function handleAcceptInvitationOnProject(): void
    {
        $invitationActivity = ActivitypubActivityCreator::acceptContributionActivity($this->actor->get("id"),$this->payload['objectId']);
        Handler::handle($invitationActivity, $this->actorObject);
    }
    public function handleLeaveContribution(): void
    {
        $undoContributorActivity = ActivitypubActivityCreator::leaveContributionOnProjectActivity($this->actor, $this->subject, $this->payload);
        Handler::handle($undoContributorActivity, $this->actorObject);
    }
    public function handleAddAsAdminInProject(): void
    {
        $addAsAdminActivity = ActivitypubActivityCreator::createOfferProjectActivity($this->payload);
        Handler::handle($addAsAdminActivity, $this->actorObject);
    }
    public function handleDeleteContribution(): void
    {
        $addAsAdminActivity = ActivitypubActivityCreator::createDeleteContributionOnProjectActivity($this->payload);
        Handler::handle($addAsAdminActivity, $this->actorObject);
    }
    public function handleFollowProject(): void
    {
         $activity = ActivitypubActivityCreator::createFollowActivity($this->subject);
        Handler::handle($activity, $this->actorObject);
    }
    public function handleLeaveFollow(): void
    {
        $undoFollowActivity = ActivitypubActivityCreator::leaveFollowInProjectActivity($this->actor, $this->subject, $this->payload);
        Handler::handle($undoFollowActivity, $this->actorObject);
    }

    public function handleAddTag(): void
    {

        ActivitypubLink::saveTags($this->payload,$this->actorObject);
    }
}
