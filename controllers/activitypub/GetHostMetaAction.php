<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use Rest;

class GetHostMetaAction extends \PixelHumain\PixelHumain\components\Action{
	public function run() {
        header("Content-Type: application/xml+xrd");
        echo '<?xml version="1.0" encoding="UTF-8"?>
                <XRD xmlns:hm="http://host-meta.net/xrd/1.0" xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
                    <link rel="lrdd" type="application/xrd+xml" template="https://'.Config::HOST().'/.well-known/webfinger?xrd?uri={uri}"/>
                    <link rel="lrdd" type="application/json" template="https://'.Config::HOST().'/.well-known/webfinger?resource={uri}"/>	
                </XRD>
            ';
	}
}
