<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use Rest;
use Organization;
class GetCommunityAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($userId, $type,$actor) {
        $oppositeType = ($type === "follows") ? "followers":"follows";
        $actorType = $actor == "group"  ? Organization::COLLECTION : Person::COLLECTION;
        $user = PHDB::findOneById($actorType, $userId);
        if(!$user || !isset($user["links"]["activitypub"][$type]))
            return Rest::json([]);
        $res = [];
        foreach($user["links"]["activitypub"][$type] as $ref => $link){
            $data = [
                $type => $link
            ];
            if(isset($user["links"]["activitypub"][$oppositeType][$ref]))
                $data[$oppositeType] = $user["links"]["activitypub"][$oppositeType][$ref];
            $value = Type::createFromAnyValue($link["invitorId"]);
            if($value != null){
                $res[] = [
                    "link" => $data,
                    "data" => (Type::createFromAnyValue($link["invitorId"]))->toArray()
                ];
            }
        }
        return Rest::json($res);
	}
}
