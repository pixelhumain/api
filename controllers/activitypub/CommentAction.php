<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use DateTime;
use News;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use Rest;

class CommentAction extends \PixelHumain\PixelHumain\components\Action{
	public function run() {
        if(!isset($_POST["parentId"]) || !isset($_POST["comment"]))
            return Rest::json(["error"=>true, "msg"=>"Missing parameters."]);

        $news = PHDB::findOneById(News::COLLECTION, $_POST["parentId"]);
        if(!$news || !isset($news["objectUUID"]))
            return Rest::json(["error"=>true, "msg"=>"Invalid parameters."]);

        $userConnected = ActivitypubActor::getCoPersonAsActor($_SESSION["user"]["username"]);

        $note = ActivitypubObject::getObjectByUUID($news["objectUUID"]);



        $commentParams = [
            "attributedTo" => $userConnected->get("id"),
            "content" => $_POST["comment"],
            "inReplyTo" => $note->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [Utils::PUBLIC_INBOX],
            "cc" => [
                $note->get("attributedTo")
            ]
        ];
        if (filter_var($news["objectUUID"], FILTER_VALIDATE_URL)) {
           $commentParams['cc'] = $note->get('cc');
        }
        $comment = Type::create("Note", $commentParams);

        $activity = Type::create("Create", [
            "actor" => $userConnected->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601)
        ]);
        $activity->set("object", $comment);
        $activity->set("to", $comment->get("to"));
        $activity->set("cc", $comment->get("cc"));

        Handler::handle($activity);
        return Rest::json(["error" => false, "result"=> array('message' => 'ok'), "msg" => 'Opération réussie avec succès.']);
	}
}
