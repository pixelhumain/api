<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use Exception;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Request;
use Rest;
use Yii;

class DoActivityAction extends \PixelHumain\PixelHumain\components\Action{
	public function run() {
        try{
            $data = json_decode(Yii::app()->request->getRawBody(), true);
			$activity =  Type::createFromAnyValue($data);
			Handler::handle($activity);
		}catch(Exception $e){
			return Rest::json(["error"=> $e->getMessage()]);
		}
	}
}
