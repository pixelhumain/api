<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use Rest;
use PHDB;
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("HTTP/1.1 200 OK");
    exit;
}

/*
 * http://communecter74-dev/api/activitypub/getobjectcountbytype?startDate=2023-05-06T15:35:30.000Z&endDate=2023-07-16T09:04:28.000Z
 */
class GetObjectCountByTypeAction extends   \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        try {
            $startDate = $_GET["startDate"];
            $endDate = $_GET["endDate"];
            $domain = isset($_GET[ "domain" ]) ? $_GET[ "domain" ] : Config::SCHEMA() . '://' . Config::HOST();
            $output = ActivitypubObject::getActivityByDateRange($startDate, $endDate);

           return Rest::json([
               'rows' => $output,
               'dates' => array('from' => $startDate, 'to' => $endDate),
               'domain' => $domain,
               "count"=>count($output)
           ]);

        }
    	catch (\Exception $e) {
            return Rest::json(["error"=> $e->getMessage()]);
        }
    }
}
