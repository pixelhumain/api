<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use Rest;
use PHDB;
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("HTTP/1.1 200 OK");
    exit;
}


/**
 * http://communecter74-dev/api/activitypub/getinteractions?startDate=2023-05-06T15:35:30.000Z&endDate=2023-07-16T09:04:28.000Z&refdomain=communecter.org&domain=8458-154-126-9-199.ngrok-free.app
 */
class GetInteractionByDateRangeAction extends   \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        try {
            $startDate = isset($_GET[ "startDate" ]) ? $_GET[ "startDate" ] : '';
            $endDate = isset($_GET[ "endDate" ]) ? $_GET[ "endDate" ] : '';
            $domain = $_GET["domain"];
            $refdomain = $_GET["refdomain"];
            $output = ActivitypubActivity::getInteractionByDateRange($startDate, $endDate, $domain,$refdomain);
            $rows=[];
            foreach ($output as $key => $value) {
                $rows[] = $value;
            }
           return Rest::json([
               'rows' => $rows,
               'dates' => array('from' => $startDate, 'to' => $endDate),
               "count"=>count($output)
           ]);
        }
    	catch (\Exception $e) {
            return Rest::json(["error"=> $e->getMessage()]);
        }
    }
}