<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use Rest;
use PHDB;
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("HTTP/1.1 200 OK");
    exit;
}

class GetInstanceInteractingByDateRangeAction extends   \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        try {
            $startDate = isset($_GET[ "startDate" ]) ? $_GET[ "startDate" ] : '';
            $endDate = isset($_GET[ "endDate" ]) ? $_GET[ "endDate" ] : '';
            $output = ActivitypubActivity::getInstanceInteractingByDateRange($startDate, $endDate);
            $rows=[];
            foreach ($output as $key => $value) {
                $rows[]= Utils::getWebsiteDomain($value);
            }
           return Rest::json([
               'rows' => $rows,
               'dates' => array('from' => $startDate, 'to' => $endDate),
               "count"=>count($output)
           ]);
        }
    	catch (\Exception $e) {
            return Rest::json(["error"=> $e->getMessage()]);
        }
    }
}