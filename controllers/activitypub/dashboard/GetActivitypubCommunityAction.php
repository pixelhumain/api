<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard;
use Person;
use Rest;
use PHDB;
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("HTTP/1.1 200 OK");
    exit;
}

class GetActivitypubCommunityAction extends   \PixelHumain\PixelHumain\components\Action
{
    public function run() {
       $output = PHDB::find(Person::COLLECTION,["preferences.activitypub" => true]);
       $rows=[];
       foreach ($output as $key => $value){
            $rows[] = array('id'=> $key,'name' => $value['name'],'geo' => isset($value['geo']) ? $value['geo']: null);
       }
       return Rest::json([
           'rows' => $rows,
           "count"=>count($output)
       ]);
    }
}