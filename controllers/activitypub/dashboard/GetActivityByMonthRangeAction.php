<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\dashboard;
use Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use Rest;
use DateTime;
use DateInterval;
use DatePeriod;
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("HTTP/1.1 200 OK");
    exit;
}

/*
 * http://communecter74-dev/api/activitypub/getactivitybymonthrange?startDate=2023-05-06T15:35:30.000Z&endDate=2023-07-16T09:04:28.000Z
 */
class GetActivityByMonthRangeAction extends   \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        try {
            $startDate = $_GET["startDate"];
            $endDate = $_GET["endDate"];
            $domain = isset($_GET[ "domain" ]) ? $_GET[ "domain" ] : Config::SCHEMA() . '://' . Config::HOST();
            $type = isset($_GET[ "type" ]) ? $_GET["type"] : "all";
            $refDomain = isset($_GET[ "refDomain" ]) ? $_GET["refDomain"] : "all";
            $output = ActivitypubActivity::getActivityByDateRangeAndType($startDate, $endDate,$domain,$type,$refDomain);

           return Rest::json([
               'rows' => self::groupDataByMonth($output,$startDate,$endDate),
               'dates' => array('from' => $startDate, 'to' => $endDate),
               'domain' => $domain,
               "count"=>null
           ]);
        }
    	catch (\Exception $e) {
            return Rest::json(["error"=> $e->getMessage()]);
        }
    }
    private function groupDataByMonth($data, $startDate, $endDate)
    {
        $start = new DateTime($startDate);
        $end = new DateTime($endDate);

        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $monthsArray = array();
        foreach ($period as $date) {
            $month = $date->format('M');
            $monthsArray[] = $month;
        }

        $groupedData = array();
        foreach ($monthsArray as $month) {
            $groupedData['in'][$month] = array();
            $groupedData['out'][$month] = array();
        }

        foreach ($data['in'] as $record) {
            $date = $record['createAt']->toDateTime();
            $monthKey = $date->format('M');
            $groupedData['in'][$monthKey][] = $record;
        }
        foreach ($data['out'] as $record) {
            $date = $record['createAt']->toDateTime();
            $monthKey = $date->format('M');
            $groupedData['out'][$monthKey][] = $record;
        }


        return array(
            "labels" =>$monthsArray,
            "groupedData" => $groupedData
        );
    }


}
