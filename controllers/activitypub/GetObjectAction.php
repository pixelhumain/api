<?php
namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubObject;
use Rest;

class GetObjectAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($id) {
        $res = ActivitypubObject::getObjectByUUID($id);
        if(!$res)
            return Rest::json(["error" => "Activity not found"]);
        return Rest::json($res->toArray(), null, true, 'application/activity+json; charset=utf-8');
	}
}
