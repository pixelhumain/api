<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\test;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;

class NotificationGenerator
{
    public static function getActivityDescription($activity)
    {
        $description = '';

        // Vérifie si le champ "summary" est défini
        if (isset($activity['summary'])) {
            $description = $activity['summary'];
        } else {
            // Vérifie si le champ "actor" est défini
            if (isset($activity['actor']['name'])) {
                $actor = $activity['actor']['name'];
            }else{
                $res = Type::createFromAnyValue($activity['actor']);
                $actor =$res->get('preferredUsername');
            }
            if (isset($activity['name'])) {
                $activityName = $activity['name'];
            }
            // Vérifie si le champ "object" est défini
            if (isset($activity['object']['name'])) {
                $object = $activity['object']['name'];
            }
            if (isset($activity['object']['type'])) {
                $objectType = $activity['object']['type'];
            }

            // Vérifie si le type de l'objet est "Image"
            if (isset($activity['object']['type']) && $activity['object']['type'] === 'Image') {
                // Vérifie si l'URL de l'image est définie
                if (isset($activity['object']['url'])) {
                    $imageUrl = $activity['object']['url'];
                    $description .= " de $imageUrl";
                }
            }
            // Vérifie si le champ "target" est défini
            if (isset($activity['target'])) {
                if (is_array($activity['target'])) {
                    $targets = array();
                    foreach ($activity['target'] as $target) {
                        if (isset($target['name'])) {
                            $targets[] = $target['name'];
                        }
                    }
                    if (!empty($targets)) {
                        $targetList = implode(' et ', $targets);
                        $target = $targetList;
                    }
                } elseif (is_object($activity['target'])) {
                    if (isset($activity['target']['name'])) {
                        $target = $activity['target']['name'];
                    }
                }
            }


            // Vérifie si le champ "location" est défini
            if (isset($activity['location']['name'])) {
                $location = $activity['location']['name'];
            }

            // Vérifie si le champ "origin" est défini pour l'activité de suppression de note
            if (isset($activity['origin']) && isset($activity['type'])) {
                if (isset($activity['origin']['name']) && isset($activity['origin']['type'])) {

                    $originName = $activity['origin']['name'];
                    $originType = $activity['origin']['type'];
                    $origin = self::getObjectType($originType) . " " . $originName;
                }
            }
            // Vérifie si le champ "oneOf" est défini
            if (isset($activity['oneOf'])) {
                $options = array();
                foreach ($activity['oneOf'] as $option) {
                    if (isset($option['name'])) {
                        $options[] = $option['name'];
                    }
                }
                if (!empty($options)) {
                    $optionList = implode(' ou ', $options);
                    $oneOf = "Choisissez : $optionList";
                }
            }
            // Génère la description en utilisant les noms de l'acteur, de l'objet, de la cible, du lieu et de l'origine
            if (isset($actor)) {
                if (isset($object)) {
                    $action = self::getActivityAction($activity['type']);
                    $objectType = self::getObjectType($objectType);
                    $description = "$actor $action";
                    if (isset($target)) {
                        $description .= " dans $target ";
                        $description .=  "'$object'";
                    } else {
                        $description .=  "' $object'";
                    }
                    if (isset($origin)) {
                        $description .= " depuis $origin ";
                    }
                } elseif (isset($activity['type'])) {

                    $action = self::getActivityAction($activity['type']);
                    if (isset($activity['target']['type'])) {
                        $objectType = self::getObjectType($activity['target']['type']);
                    }
                    $description = "$actor $action";
                    if (isset($target)) {
                        $description .= " dans $target ";
                    }
                    if (isset($activityName)) {
                        $description .= " '$activityName' ";
                    }
                    if (isset($origin)) {
                        $description .= " depuis $origin ";
                    }

                    if (isset($objectType)) {
                        $description .=  " $objectType";
                    }
                    if (isset($oneOf)) {
                        $description .=  " $oneOf";
                    }
                } else {
                    $description .= " a effectué une activité non reconnue.";
                }
                if (isset($activity['object']['object'])) {
                    // Vérifie si le champ "name" de l'objet "Invite" est défini
                    if (isset($activity['object']['object']['name']) && isset($activity['object']['object']['type'])) {
                        $inviteObjectName = $activity['object']['object']['name'];
                        $inviteObjectType = self::getObjectType($activity['object']['object']['type']);
                        $description .= " $inviteObjectType : '$inviteObjectName'";
                    }
                }
                if (isset($activity['object']['target'])) {
                    // Vérifie si le champ "name" de l'objet "Invite" est défini
                    if (isset($activity['object']['target']['name']) && isset($activity['object']['target']['type'])) {
                        $targetObjectName = $activity['object']['target']['name'];
                        $targetObjectType = self::getObjectType($activity['object']['target']['type']);
                        $description .= " dans $targetObjectType : '$targetObjectName'";
                    }
                }
                if (isset($activity['object']['location'])) {
                    // Vérifie si le champ "name" de l'objet "Invite" est défini
                    if (isset($activity['object']['location']['name']) && isset($activity['object']['location']['type'])) {
                        $locationObjectName = $activity['object']['location']['name'];
                        $locationObjectType = self::getObjectType($activity['object']['location']['type']);
                        $description .= " à '$locationObjectName'";
                    }
                }
            } else {
                $description = "Activité non reconnue.";
            }
        }

        return $description;
    }
    public static function getObjectType($type)
    {
        // Tableau de traductions pour les types d'objet
        $translations = array(
            'Note' => 'une note',
            'Person' => '',
            'Image' => 'une image',
            'Invite' => 'une invitation',
            'Event' => 'une evenement',
            'Collection' => 'dans sa collection',
            'Group' => ' groupe ',
            'Offer' => 'offre ',
            'Article' => 'Article',
            'Place' => 'place',
            'Audio' => 'audio',
            'Project' => 'Projet'
            // Ajoutez d'autres traductions pour les types d'objet supplémentaires
        );

        // Vérifie si une traduction existe pour le type d'objet donné
        if (isset($translations[$type])) {
            return $translations[$type];
        } else {
            return 'une activité';
        }
    }

    public static function getActivityAction($type)
    {
        // Tableau de traductions pour les types d'activité
        $translations = array(
            'Create' => 'a créé ',
            'Delete' => 'a supprimé ',
            'Remove' => 'a supprimé ',
            'Ignore' => 'a ignoré ',
            'Accept' => 'a accepté ',
            'TentativeReject' => 'a tenté de rejeté ',
            'Leave' => 'a quitté ',
            'Like' => ' aime ',
            'Offer' => 'offre ',
            'Invite' => 'à invité ',
            'Reject' => 'a réjeté ',
            'Undo' => 'a annulé ',
            'Update' => 'mis à jour ',
            'View' => 'a vue',
            'Listen' => 'a ecouté',
            'Read' => 'a lu',
            'Move' => 'a deplacé',
            "Travel" => 'went',
            'Announce' => 'annonced',
            'Block' => 'blocked ',
            'Flag' => 'flagged',
            'Dislike' => 'disliked',
            'Question' => 'Ask Question',
            'Add' => 'ajouté',
            "Join" => 'rejoint'

            // Ajoutez d'autres traductions pour les types d'activité supplémentaires
        );

        // Vérifie si une traduction existe pour le type d'activité donné
        if (isset($translations[$type])) {
            $action = $translations[$type];
            return "$action";
        } else {
            return 'a effectué';
        }
    }
}
