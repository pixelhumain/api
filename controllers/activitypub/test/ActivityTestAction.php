<?php

namespace PixelHumain\PixelHumain\modules\api\controllers\activitypub\test;

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\Type;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use Rest;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;

class ActivityTestAction extends \PixelHumain\PixelHumain\components\Action
{
    public function __construct($id, $controller, $config)
    {
        parent::__construct($id, $controller, $config);
    }

    public function run()
    {
        $actor =  "https://48c1-102-16-42-132.ngrok-free.app/api/activitypub/users/u/ArmelWanes";
        $object=Type::createFromAnyValue("https://48c1-102-16-42-132.ngrok-free.app/api/activitypub/projects/p/putain");
        $activity = ActivitypubActivityCreator::createInvitationProjectActivity($object,$actor,"becomeadmin");
        $message = NotificationGenerator::getActivityDescription($activity->toArray());
        ActivitypubActivity::save($activity,[],[]);
        Handler::handle($activity, []);
      // var_dump($message);
      return Rest::json($activity->toArray());
    }
}
