<?php

namespace PixelHumain\PixelHumain\modules\api\controllers;

use PixelHumain\PixelHumain\components\ThemeHelper;
use PixelHumain\PixelHumain\modules\api\components\ApiController;
use Yii;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class DefaultController extends ApiController {

    
    public function beforeAction($action)
  	{

      parent::initPage();

      
		  return parent::beforeAction($action);
  	}

    /**
     * Home page
     */
	public function actionIndex() 
	{
    ThemeHelper::setWebsiteTheme(ThemeHelper::PH_DORI);
    $this->layout = "//layouts/mainApi";
    return $this->render("index");
  }

  
}